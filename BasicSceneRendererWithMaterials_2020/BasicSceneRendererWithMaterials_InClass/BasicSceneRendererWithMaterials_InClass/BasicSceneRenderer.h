#ifndef SCENE_RENDERER_APP_H_
#define SCENE_RENDERER_APP_H_

#include "Shaders.h"
#include "Camera.h"
#include "Entity.h"

#include <vector>

enum LightingModel {
    PER_VERTEX_DIR_LIGHT,
    BLINN_PHONG_PER_FRAGMENT_DIR_LIGHT,
    BLINN_PHONG_PER_FRAGMENT_POINT_LIGHT,
    BLINN_PHONG_PER_FRAGMENT_MULTI_LIGHT,

    NUM_LIGHTING_MODELS
};


class BasicSceneRenderer : public GLApp {

    LightingModel               mLightingModel;

    // shaders used to render entities (one program per lighting model)
    std::vector<ShaderProgram*> mPrograms;

    // graphics resources
    std::vector<Texture*>       mTextures;
    std::vector<Mesh*>          mMeshes;
    std::vector<Material*>      mMaterials;

    // scene objects
    std::vector<Entity*>        mEntities;

    Camera*                     mCamera;

    glm::mat4                   mProjMatrix;

    int                         mActiveEntityIndex;

    bool                        mVisualizePointLights;

    //
    // debug visualization
    //

    // shader used to render active entity axes
    ShaderProgram*              mDbgProgram;

    // geometry of axes
    Mesh*                       mAxes;
    // mesh values of mesh that can hav collision
    float coneMeshRadius = 2.0f;
    float coneMeshHeight = 7.0f;
    
    float playerCubeMeshWidth = 2.0f;
    float playerCubeMeshHeight = 2.0f;

    //declaring structs in a header file, you are actually supposed to give it a body along with all of its members rather than declaring it inside of .cpp and .h
    struct Obb {
        //***************************************************************************************
        //glm::vec4 c = glm::vec4(1, 3, 2, 1);//must have 1 at end parameter
        glm::vec4 c = glm::vec4();
        //glm::vec4 u[3]; //must have 0 at end parameter
        glm::vec3 u[3];
        //glm::vec4 e;
        float e[3];

        /*
        Point c;
        vector u[3];
        Vector e;
        */
    }; //C++ stucts must nd like this
    Obb obb;
    glm::vec3 closest;
    glm::vec3 closest2;
    glm::vec3 closest3;
    //moving these variables here to maintain a non variable that is sent to other methods
    glm::vec3 positionOfCone;
    glm::vec4 positionOfConeAsVectorExplicitly;
    glm::vec3 passThroughPointToCubeSpace;
    glm::vec3 passThroughPointToCubeSpace2;
    glm::vec3 passThroughPointToCubeSpace3;
    glm::vec3 splinesVector;
    bool useNextSplinesVector = false;
    glm::vec3 splinesVector2;
    glm::vec3 splinesVectorC;
    glm::vec3 splinesVectorD;
    bool useNextSplinesVectorD = false;
    int numTime = 0;
    bool hideAllCones = false;

public:
                        BasicSceneRenderer();

    void                initialize();
    void                shutdown();
    void                resize(int width, int height);
    void                draw();
    bool                update(float dt);
    void                isCollision(int mActiveEntityIndex);
    void                handleCollision(glm::vec3 pointOfCollision);
    void                ClosestPtPoint(glm::vec3* p, Obb* b, glm::vec3* q);
    void                ClosestPtPoint2(glm::vec3* p, Obb* b, glm::vec3* q);
    void                ClosestPtPoint3(glm::vec3* p, Obb* b, glm::vec3* q);
    glm::vec3           SplinePointOnCurve(float dt, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);
    glm::vec3           SplinePointOnCurveA(float dt, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);
    glm::vec3           SplinePointOnCurveC(float dt, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);
    glm::vec3           SplinePointOnCurveD(float dt, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3);

};

#endif
