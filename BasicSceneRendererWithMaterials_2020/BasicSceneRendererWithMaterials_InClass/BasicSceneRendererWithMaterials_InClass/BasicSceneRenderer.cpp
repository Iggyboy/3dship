#include "BasicSceneRenderer.h"
#include "Image.h"
#include "Prefabs.h"

#include <iostream>
#include <cmath>
//just added this
#include <glm/gtx/quaternion.hpp>

BasicSceneRenderer::BasicSceneRenderer()
    : mLightingModel(PER_VERTEX_DIR_LIGHT)
    , mCamera(NULL)
    , mProjMatrix(1.0f)
    , mActiveEntityIndex(25)//THS VARIABLE CHANGD HERE AGIAN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    , mDbgProgram(NULL)
    , mAxes(NULL)
    , mVisualizePointLights(true)
{
}

void BasicSceneRenderer::initialize()
{
    
    // print usage instructions
    std::cout << "Usage:" << std::endl;
    std::cout << "  Camera control:           WASD + Mouse" << std::endl;
    std::cout << "  Rotate active entity:     Arrow keys" << std::endl;
    std::cout << "  Reset entity orientation: R" << std::endl;
    std::cout << "  Translate active entity:  IJKLOP (world space)" << std::endl;
    std::cout << "  Translate active entity:  TFGHYU (local space)" << std::endl;
    std::cout << "  Cycle active entity:      X/Z" << std::endl;
    std::cout << "  Toggle point light vis.:  Tab" << std::endl;
    std::cout << "  press q to toggle hide enemy vision" << std::endl;

    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    // enable blending (needed for textures with alpha channel)
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    mPrograms.resize(NUM_LIGHTING_MODELS);

    mPrograms[PER_VERTEX_DIR_LIGHT] = new ShaderProgram("shaders/PerVertexDirLight-vs.glsl",
                                                                "shaders/PerVertexDirLight-fs.glsl");
    
    mPrograms[BLINN_PHONG_PER_FRAGMENT_DIR_LIGHT] = new ShaderProgram("shaders/BlinnPhongPerFragment-vs.glsl",
                                                                      "shaders/BlinnPhongPerFragmentDirLight-fs.glsl");

    mPrograms[BLINN_PHONG_PER_FRAGMENT_POINT_LIGHT] = new ShaderProgram("shaders/BlinnPhongPerFragment-vs.glsl",
                                                                        "shaders/BlinnPhongPerFragmentPointLight-fs.glsl");

    mPrograms[BLINN_PHONG_PER_FRAGMENT_MULTI_LIGHT] = new ShaderProgram("shaders/BlinnPhongPerFragment-vs.glsl",
                                                                        "shaders/BlinnPhongPerFragmentMultiLight-fs.glsl");

    //
    // Create meshes
    //

    mMeshes.push_back(CreateTexturedCube(1));
    mMeshes.push_back(CreateChunkyTexturedCylinder(0.5f, 1, 8));
    mMeshes.push_back(CreateSmoothTexturedCylinder(0.5f, 1, 15));

    float roomWidth = 32;
    float roomHeight = 24;
    float roomDepth = 52;
    float roomTilesPerUnit = 0.25f;

    // front and back walls
    Mesh* fbMesh = CreateTexturedQuad(roomWidth, roomHeight, roomWidth * roomTilesPerUnit, roomHeight * roomTilesPerUnit);
    mMeshes.push_back(fbMesh);
    // left and right walls
    Mesh* lrMesh = CreateTexturedQuad(roomDepth, roomHeight, roomDepth * roomTilesPerUnit, roomHeight * roomTilesPerUnit);
    mMeshes.push_back(lrMesh);
    // ceiling and floor
    Mesh* cfMesh = CreateTexturedQuad(roomWidth, roomDepth, roomWidth * roomTilesPerUnit, roomDepth * roomTilesPerUnit);
    mMeshes.push_back(cfMesh);

    //
    // Load textures
    //

    std::vector<std::string> texNames;
    texNames.push_back("textures/CarvedSandstone.tga");
    texNames.push_back("textures/rocky.tga");
    texNames.push_back("textures/bricks_overpainted_blue_9291383.tga");
    texNames.push_back("textures/water_drops_on_metal_3020602.tga");
    texNames.push_back("textures/skin.tga");
    texNames.push_back("textures/white.tga");
    texNames.push_back("textures/yo.tga");
    texNames.push_back("textures/black.tga");

    for (unsigned i = 0; i < texNames.size(); i++)
        mTextures.push_back(new Texture(texNames[i], GL_REPEAT, GL_LINEAR));

    //
    // Create materials
    //

    // add a material for each loaded texture (with default tint)
    for (unsigned i = 0; i < texNames.size(); i++)
        mMaterials.push_back(new Material(mTextures[i]));

    //
    // set extra material properties
    //

    // water drops (sharp and strong specular highlight)
    mMaterials[3]->specular = glm::vec3(1.0f, 1.0f, 1.0f);
    mMaterials[3]->shininess = 128;

    // skin (washed out and faint specular highlight)
    mMaterials[4]->specular = glm::vec3(0.3f, 0.3f, 0.3f);
    mMaterials[4]->shininess = 8;

    // white
    mMaterials[5]->specular = glm::vec3(0.75f, 0.75f, 0.75f);
    mMaterials[5]->shininess = 64;

    // yo
    mMaterials[6]->specular = glm::vec3(1.0f, 0.0f, 1.0f);  // magenta highlights
    mMaterials[6]->shininess = 16;

    // black
    mMaterials[7]->specular = glm::vec3(1.0f, 0.5f, 0.0f);  // orange hightlights
    mMaterials[7]->shininess = 16;

    //
    // Create entities
    //



    unsigned numRows = mMaterials.size();
    float spacing = 3;
    float z = 0.5f * spacing * numRows;
    for (unsigned i = 2; i < mMaterials.size(); i++) {
        // cube
        mEntities.push_back(new Entity(mMeshes[0], mMaterials[i], Transform(-4.0f, 0.0f, z)));
        //std::cout << "  mEntty[0] which is a cylinder should be (-4,0,12) but it is: (" << mEntities[0]->getPosition().x << ", " << mEntities[0]->getPosition().y << ", " << mEntities[0]->getPosition().z << ")" << std::endl;

        // chunky cylinder
        mEntities.push_back(new Entity(mMeshes[1], mMaterials[i], Transform( 0.0f, 0.0f, z)));
        //std::cout << "  mEntty[1] which is a cylinder should be (0,0,12) but it is: (" << mEntities[1]->getPosition().x << ", " << mEntities[1]->getPosition().y << ", " << mEntities[1]->getPosition().z << ")" << std::endl;

        // smooth cylinder
        mEntities.push_back(new Entity(mMeshes[2], mMaterials[i], Transform( 4.0f, 0.0f, z)));
        //std::cout << "  mEntty[2] which is a cylinder should be (4,0,12) but it is: (" << mEntities[2]->getPosition().x << ", " << mEntities[2]->getPosition().y << ", " << mEntities[2]->getPosition().z << ")" << std::endl;


        // next row
        z -= spacing;
    }

    //
    // Create room
    //
    //std::cout << "  mEntty count til start of mEntty[3]: (" << mEntities.size() << std::endl;

    // back wall
    mEntities.push_back(new Entity(fbMesh, mMaterials[1], Transform(0, 0, -0.5f * roomDepth)));
    //std::cout << "  mEntty[18] which is a wall should be (0,0,-26) but it is: (" << mEntities[18]->getPosition().x << ", " << mEntities[18]->getPosition().y << ", " << mEntities[18]->getPosition().z << ")" << std::endl;

    // front wall
    mEntities.push_back(new Entity(fbMesh, mMaterials[1], Transform(0, 0, 0.5f * roomDepth, glm::angleAxis(glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f)))));
    //std::cout << "  mEntty[19] which is a wall should be (0,0,26) but it is: (" << mEntities[19]->getPosition().x << ", " << mEntities[19]->getPosition().y << ", " << mEntities[19]->getPosition().z << ")" << std::endl;

    // left wall
    mEntities.push_back(new Entity(lrMesh, mMaterials[1], Transform(-0.5f * roomWidth, 0, 0, glm::angleAxis(glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)))));
    //std::cout << "  mEntty[20] which is a wall should be (-16,0,0) but it is: (" << mEntities[20]->getPosition().x << ", " << mEntities[20]->getPosition().y << ", " << mEntities[20]->getPosition().z << ")" << std::endl;

    // right wall
    mEntities.push_back(new Entity(lrMesh, mMaterials[1], Transform(0.5f * roomWidth, 0, 0, glm::angleAxis(glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f)))));
    //std::cout << "  mEntty[21] which is cone should be (16,0,0) but it is: (" << mEntities[21]->getPosition().x << ", " << mEntities[21]->getPosition().y << ", " << mEntities[21]->getPosition().z << ")" << std::endl;

    // floor
    mEntities.push_back(new Entity(cfMesh, mMaterials[0], Transform(0, -0.5f * roomHeight, 0, glm::angleAxis(glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f)))));
    //std::cout << "  mEntty[22] which is cone should be (0,-12,0) but it is: (" << mEntities[22]->getPosition().x << ", " << mEntities[22]->getPosition().y << ", " << mEntities[22]->getPosition().z << ")" << std::endl;

    // ceiling
    mEntities.push_back(new Entity(cfMesh, mMaterials[0], Transform(0, 0.5f * roomHeight, 0, glm::angleAxis(glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)))));
    //std::cout << "  mEntty[23] should be (0,12,0) but it is: (" << mEntities[23]->getPosition().x << ", " << mEntities[23]->getPosition().y << ", " << mEntities[23]->getPosition().z << ")" << std::endl;

    //std::cout << "  mEntty count til start of mEntty[9]: (" << mEntities.size() << std::endl;


    //mak cn
    /*
    // make sight circle around ship
    std::vector<glsh::VPNC> shipsSightCircleVerts;
    std::vector<unsigned short> shipSightCircleIndices;
    // make lines for sight point of ship
    std::vector<glsh::VPNC> shipsSightCircleLinesVerts;
    std::vector<unsigned short> shipSightCircleLinesIndices;




    
    //glBegin(GL_LINE_LOOP);
    //glColor3f(1.0f, 1.0f, 0.0f);
    int numSegments1 = 24;
    float r1 = 2.0f;
    float angleStep1 = 2 * 3.14159265359f / numSegments1;

    //glVertex3f(0,-0.3f, 0.0f);
    for (int i = 0; i < numSegments1; i++) {
        float angle = i * angleStep1;
        float x = r1 * cos(angle);
        float y = r1 * sin(angle);
        glVertex3f(0, x - 2.0, y);
     
        // sight circle of ship
        shipsSightCircleVerts.push_back(glsh::VPNC(0.0f + 7.0f, x - 0.3f, y, 0.0f, 0.0f, 1.0f, 1.0f, 0.8f, 0.4f, 1.0f));  // front
        shipSightCircleIndices.push_back(i);

        
        if (i == (numSegments1-1)) {
            //shipFaceDirectionCircleIndices.push_back(0);
        }
        else {
            shipFaceDirectionCircleIndices.push_back(i);
        }
        
       
        //lines for sight of ship
        shipsSightCircleLinesVerts.push_back(glsh::VPNC(0.0f + 7.0f, x - 0.3f, y, 0.0f, 0.0f, 1.0f, 1.0f, 0.8f, 0.4f, 1.0f));  // front
        shipsSightCircleLinesVerts.push_back(glsh::VPNC(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.8f, 0.4f, 1.0f));  // front
    }
    //for sight
    mShipSightCircleMesh = glsh::CreateMesh(GL_LINE_LOOP, shipsSightCircleVerts, shipSightCircleIndices);
    mShipSightCircleLinesMesh = glsh::CreateMesh(GL_LINES, shipsSightCircleLinesVerts);

    */
    // radius = 2.0f    , height = 7.0f
    Mesh* coneMesh = CreateChunkyCone(coneMeshRadius, coneMeshHeight, 24);
    //CreateTexturedQuad(roomWidth, roomHeight, roomWidth * roomTilesPerUnit, roomHeight * roomTilesPerUnit);

    mEntities.push_back(new Entity(coneMesh, mMaterials[0], Transform(0.0f, 0.0f, 7))); 
    //pos x axs is right, pos y axis is up,pos z axs in the direction of the camera
    //std::cout << "_________  mEntty[24] which is cone should be (0,0,7) but it is: (" << mEntities[24]->getPosition().x << ", " << mEntities[24]->getPosition().y << ", " << mEntities[24]->getPosition().z << ")" << std::endl;

    mEntities[24]->rotate(270.0f, 1.0f, 0.0f, 0.0f);
    //glm::mat4 modelMatrixForEnemy = glm::translate(glm::mat4(1.0f), glm::vec3(enemySelectedXvalue, enemySelectedYvalue, enemySelectedZvalue));

    //width = 2.0f
    Mesh* playerCubeMesh = CreateSolidCube(playerCubeMeshWidth);
    mEntities.push_back(new Entity(playerCubeMesh, mMaterials[5], Transform(0.0f, 0.0f, 13)));

    //std::cout << "____________  mEntty[25] which is cube should be (0,0,13) but it is: (" << mEntities[25]->getPosition().x << ", " << mEntities[25]->getPosition().y << ", " << mEntities[25]->getPosition().z << ")"<< std::endl;
    
    Mesh* coneMesh2 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMesh2, mMaterials[0], Transform(2.0f, 0.0f, 7)));
    //pos x axs is right, pos y axis is up,pos z axs in the direction of the camera
    //std::cout << "_________  mEntty[26] which is cone should be (0,0,7) but it is: (" << mEntities[26]->getPosition().x << ", " << mEntities[26]->getPosition().y << ", " << mEntities[26]->getPosition().z << ")" << std::endl;





    //For the splines
    //mEntity[27]
    Mesh* coneMeshForSplines0 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines0, mMaterials[0], Transform(-16.2764f, -11.2951f, -22.7669f)));

    //mEntity[28]
    Mesh* coneMeshForSplines1 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines1, mMaterials[0], Transform(-15.351f, -11.2951f, 11.0337f)));

    //mEntity[29]
    Mesh* coneMeshForSplines2 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines2, mMaterials[0], Transform(15.351f, -11.2951f, 11.0337f)));

    //mEntity[30]
    Mesh* coneMeshForSplines3 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines3, mMaterials[0], Transform(16.2764f, -11.2951f, -22.7669f)));


    //mEntity[31]
    Mesh* coneMeshForSplines4 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines4, mMaterials[0], Transform(16.2764f, -11.2951f, 22.7669f)));

    //mEntity[32]
    Mesh* coneMeshForSplines5 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines5, mMaterials[0], Transform(-16.2764f, -11.2951f, 22.7669f)));

    //----------------------for the next splines
        //mEntity[33]
    Mesh* coneMeshForSplines6 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines6, mMaterials[0], Transform(-13.9694f, -11.2951f, -4.244f)));

    //mEntity[34]
    Mesh* coneMeshForSplines7 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines7, mMaterials[0], Transform(-8.89202, -11.2951f, 0.2967f)));

    //mEntity[35]
    Mesh* coneMeshForSplines8 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines8, mMaterials[0], Transform(-2.10605f, -11.2951f, 6.07172f)));

    //mEntity[36]
    Mesh* coneMeshForSplines9 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines9, mMaterials[0], Transform(5.18545f, -11.2951f, 10.4702f)));


    //mEntity[37]
    Mesh* coneMeshForSplines10 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines10, mMaterials[0], Transform(-8.09855f, -11.2951f, 10.7999f)));

    //mEntity[38]
    Mesh* coneMeshForSplines11 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines11, mMaterials[0], Transform(4.22265f, -11.2951f, -0.0360947f)));

    //mEntity[39]
    Mesh* coneMeshForSplines12 = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMeshForSplines12, mMaterials[0], Transform(12.3887f, -11.2951f, -8.26825f)));

    //-----------------------------these are the 2 last cones
    //mEntities[40]
    Mesh* coneMeshNextCone2 = CreateChunkyCone(coneMeshRadius, coneMeshHeight, 24);
    mEntities.push_back(new Entity(coneMeshNextCone2, mMaterials[0], Transform(0.0f, 0.0f, 7)));
    mEntities[40]->rotate(270.0f, 1.0f, 0.0f, 0.0f);

    //mEntities[41]
    Mesh* coneMeshNextCone3 = CreateChunkyCone(coneMeshRadius, coneMeshHeight, 24);
    mEntities.push_back(new Entity(coneMeshNextCone3, mMaterials[1], Transform(0.0f, 0.0f, 7)));
    mEntities[41]->rotate(270.0f, 1.0f, 0.0f, 0.0f);

    //----------------------------these are the 2 last enemies
    //mEntities[42]
    Mesh* coneMesh2nd = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMesh2nd, mMaterials[0], Transform(2.0f, 0.0f, 7)));
    //pos x axs is right, pos y axis is up,pos z axs in the direction of the camera
    //std::cout << "_________  mEntty[26] which is cone should be (0,0,7) but it is: (" << mEntities[26]->getPosition().x << ", " << mEntities[26]->getPosition().y << ", " << mEntities[26]->getPosition().z << ")" << std::endl;

    //mEntities[43]
    Mesh* coneMesh3rd = CreateChunkyTexturedCylinder(0.5f, 1, 8);
    mEntities.push_back(new Entity(coneMesh3rd, mMaterials[0], Transform(2.0f, 0.0f, 7)));
    //pos x axs is right, pos y axis is up,pos z axs in the direction of the camera
    //std::cout << "_________  mEntty[26] which is cone should be (0,0,7) but it is: (" << mEntities[26]->getPosition().x << ", " << mEntities[26]->getPosition().y << ", " << mEntities[26]->getPosition().z << ")" << std::endl;











    /*
    //change positions
    mEntities[0]->setPosition(-16.2764f, -11.2951f, -22.7669f);
    std::cout << "_________  mEntty[0] which is cone should be (-16.2764f, -11.2951f, -22.7669f) but it is: (" << mEntities[0]->getPosition().x << ", " << mEntities[0]->getPosition().y << ", " << mEntities[0]->getPosition().z << ")" << std::endl;

    mEntities[1]->setPosition(-15.351f, -11.2951f, 11.0337f);
    std::cout << "_________  mEntty[1] which is cone should be (-15.351f, -11.2951f, 11.0337f) but it is: (" << mEntities[1]->getPosition().x << ", " << mEntities[1]->getPosition().y << ", " << mEntities[1]->getPosition().z << ")" << std::endl;

    mEntities[2]->setPosition(15.351f, -11.2951f, 11.0337f);
    std::cout << "_________  mEntty[1] which is cone should be (15.351f, -11.2951f, 11.0337f) but it is: (" << mEntities[2]->getPosition().x << ", " << mEntities[2]->getPosition().y << ", " << mEntities[2]->getPosition().z << ")" << std::endl;

    mEntities[3]->setPosition(16.2764f, -11.2951f, -22.7669f);
    std::cout << "_________  mEntty[1] which is cone should be (16.2764f, -11.2951f, -22.7669f) but it is: (" << mEntities[3]->getPosition().x << ", " << mEntities[3]->getPosition().y << ", " << mEntities[3]->getPosition().z << ")" << std::endl;

    */

    //
    // create the camera
    //

    mCamera = new Camera(this);
    mCamera->setPosition(12, 8, 22);
    mCamera->lookAt(0, 0, 7);
    mCamera->setSpeed(2);

    // create shader program for debug geometry
    mDbgProgram = new ShaderProgram("shaders/vpc-vs.glsl",
                                    "shaders/vcolor-fs.glsl");

    // create geometry for axes
    mAxes = CreateAxes(2);

    CHECK_GL_ERRORS("initialization");
}

void BasicSceneRenderer::shutdown()
{
    for (unsigned i = 0; i < mPrograms.size(); i++)
        delete mPrograms[i];
    mPrograms.clear();

    delete mDbgProgram;
    mDbgProgram = NULL;

    delete mCamera;
    mCamera = NULL;

    for (unsigned i = 0; i < mEntities.size(); i++)
        delete mEntities[i];
    mEntities.clear();

    for (unsigned i = 0; i < mMeshes.size(); i++)
        delete mMeshes[i];
    mMeshes.clear();

    for (unsigned i = 0; i < mMaterials.size(); i++)
        delete mMaterials[i];
    mMaterials.clear();
    
    for (unsigned i = 0; i < mTextures.size(); i++)
        delete mTextures[i];
    mTextures.clear();

    delete mDbgProgram;
    mDbgProgram = NULL;
    
    delete mAxes;
    mAxes = NULL;
}

void BasicSceneRenderer::resize(int width, int height)
{
    glViewport(0, 0, width, height);

    // compute new projection matrix
    mProjMatrix = glm::perspective(glm::radians(50.f), width / (float)height, 0.1f, 1000.0f);
}

void BasicSceneRenderer::draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // activate current program
    ShaderProgram* prog = mPrograms[mLightingModel];
    prog->activate();

    // send projection matrix
    prog->sendUniform("u_ProjectionMatrix", mProjMatrix);

    // send the texture sampler id to shader
    prog->sendUniformInt("u_TexSampler", 0);

    // get the view matrix from the camera
    glm::mat4 viewMatrix = mCamera->getViewMatrix();

    //
    // light setup depends on lighting model
    //

    if (mLightingModel == PER_VERTEX_DIR_LIGHT) {

        //----------------------------------------------------------------------------------//
        //                                                                                  //
        // Basic directional light (no ambient, specular, or emissive contributions)        //
        //                                                                                  //
        //----------------------------------------------------------------------------------//

        // direction to light
        glm::vec4 lightDir = glm::normalize(glm::vec4(1, 3, 2, 0));

        // send light direction in eye space
        prog->sendUniform("u_LightDir", glm::vec3(viewMatrix * lightDir));

        // send light color/intensity
        prog->sendUniform("u_LightColor", glm::vec3(1.0f, 1.0f, 1.0f));

    } else if (mLightingModel == BLINN_PHONG_PER_FRAGMENT_DIR_LIGHT) {

        //----------------------------------------------------------------------------------//
        //                                                                                  //
        // Directional light with ambient, specular, and emissive contributions             //
        //                                                                                  //
        //----------------------------------------------------------------------------------//

        prog->sendUniform("u_AmbientLightColor", glm::vec3(0.2f, 0.2f, 0.2f));

        // direction to light
        glm::vec4 lightDir = glm::normalize(glm::vec4(1, 3, 2, 0));

        // send light direction in eye space
        prog->sendUniform("u_LightDir", glm::vec3(viewMatrix * lightDir));

        // send light color/intensity
        prog->sendUniform("u_LightColor", glm::vec3(0.8f, 0.8f, 0.8f));

    } else if (mLightingModel == BLINN_PHONG_PER_FRAGMENT_POINT_LIGHT) {

        //----------------------------------------------------------------------------------//
        //                                                                                  //
        // Point light with ambient, specular, and emissive contributions, and attenuation  //
        //                                                                                  //
        //----------------------------------------------------------------------------------//

        prog->sendUniform("u_AmbientLightColor", glm::vec3(0.1f, 0.1f, 0.1f));

        // point light position
        glm::vec3 lightPos = glm::vec3(0, 7, 0);
        glm::vec3 lightColor = glm::vec3(1.0f, 0.9f, 0.8f);

        // send light position in eye space
        prog->sendUniform("u_LightPos", glm::vec3(viewMatrix * glm::vec4(lightPos, 1)));

        // send light color/intensity
        prog->sendUniform("u_LightColor", lightColor);

        prog->sendUniform("u_AttQuat", 0.005f);
        prog->sendUniform("u_AttLin", 0.05f);
        prog->sendUniform("u_AttConst", 1.0f);

        // render the light as an emissive cube, if desired
        if (mVisualizePointLights) {
            const Mesh* lightMesh = mMeshes[0];
            lightMesh->activate();
            glBindTexture(GL_TEXTURE_2D, mTextures[7]->id());  // use black texture
            prog->sendUniform("u_MatEmissiveColor", lightColor);
            //Translate makes a matrix that will translate the vector that u multiply it with**********************************************
            prog->sendUniform("u_ModelviewMatrix", glm::translate(viewMatrix, glm::vec3(lightPos)));
            prog->sendUniform("u_NormalMatrix", glm::mat3(1.0f));
            lightMesh->draw();
        }

    } else if (mLightingModel == BLINN_PHONG_PER_FRAGMENT_MULTI_LIGHT) {

        //----------------------------------------------------------------------------------//
        //                                                                                  //
        // Multiple directional/point lights                                                //
        //                                                                                  //
        //----------------------------------------------------------------------------------//

        prog->sendUniform("u_AmbientLightColor", glm::vec3(0.1f, 0.1f, 0.1f));

        prog->sendUniformInt("u_NumDirLights", 1);
        prog->sendUniformInt("u_NumPointLights", 3);

        // directional light
        glm::vec4 lightDir = glm::normalize(glm::vec4(1, 3, 2, 0));
        prog->sendUniform("u_DirLights[0].dir", glm::vec3(viewMatrix * lightDir));
        prog->sendUniform("u_DirLights[0].color", glm::vec3(0.3f, 0.3f, 0.3f));

        // point light
        glm::vec3 lightPos1 = glm::vec3(-7, 5, -12);
        glm::vec3 lightColor1 = glm::vec3(1.0f, 0.0f, 0.0f);
        prog->sendUniform("u_PointLights[0].pos", glm::vec3(viewMatrix * glm::vec4(lightPos1, 1)));
        prog->sendUniform("u_PointLights[0].color", lightColor1);
        prog->sendUniform("u_PointLights[0].attQuat", 0.01f);
        prog->sendUniform("u_PointLights[0].attLin", 0.1f);
        prog->sendUniform("u_PointLights[0].attConst", 1.0f);

        // point light
        glm::vec3 lightPos2 = glm::vec3(7, 5, -12);
        glm::vec3 lightColor2 = glm::vec3(0.0f, 0.0f, 1.0f);
        prog->sendUniform("u_PointLights[1].pos", glm::vec3(viewMatrix * glm::vec4(lightPos2, 1)));
        prog->sendUniform("u_PointLights[1].color", lightColor2);
        prog->sendUniform("u_PointLights[1].attQuat", 0.01f);
        prog->sendUniform("u_PointLights[1].attLin", 0.1f);
        prog->sendUniform("u_PointLights[1].attConst", 1.0f);

        // point light
        glm::vec3 lightPos3 = glm::vec3(-7, -5, 15);
        glm::vec3 lightColor3 = glm::vec3(0.0f, 1.0f, 0.0f);
        prog->sendUniform("u_PointLights[2].pos", glm::vec3(viewMatrix * glm::vec4(lightPos3, 1)));
        prog->sendUniform("u_PointLights[2].color", lightColor3);
        prog->sendUniform("u_PointLights[2].attQuat", 0.01f);
        prog->sendUniform("u_PointLights[2].attLin", 0.1f);
        prog->sendUniform("u_PointLights[2].attConst", 1.0f);

        // render the point lights as emissive cubes, if desirable
        if (mVisualizePointLights) {
            glBindTexture(GL_TEXTURE_2D, mTextures[7]->id());  // use black texture
            prog->sendUniform("u_NormalMatrix", glm::mat3(1.0f));
            const Mesh* lightMesh = mMeshes[0];
            lightMesh->activate();
            prog->sendUniform("u_MatEmissiveColor", lightColor1);
            prog->sendUniform("u_ModelviewMatrix", glm::translate(viewMatrix, glm::vec3(lightPos1)));
            lightMesh->draw();
            prog->sendUniform("u_MatEmissiveColor", lightColor2);
            prog->sendUniform("u_ModelviewMatrix", glm::translate(viewMatrix, glm::vec3(lightPos2)));
            lightMesh->draw();
            prog->sendUniform("u_MatEmissiveColor", lightColor3);
            prog->sendUniform("u_ModelviewMatrix", glm::translate(viewMatrix, glm::vec3(lightPos3)));
            lightMesh->draw();
        }
    }

    // render all entities
    for (unsigned i = 0; i < mEntities.size(); i++) {

        if (i == 24 || i == 40 || i == 41) {

            if (hideAllCones == true) {
                Entity* ent = mEntities[i];

                // use the entity's material
                const Material* mat = ent->getMaterial();
                glBindTexture(GL_TEXTURE_2D, mat->tex->id());   // bind texture
                prog->sendUniform("u_Tint", mat->tint);     // send tint color

                // send the Blinn-Phong parameters, if required
                if (mLightingModel > PER_VERTEX_DIR_LIGHT) {
                    prog->sendUniform("u_MatEmissiveColor", mat->emissive);
                    prog->sendUniform("u_MatSpecularColor", mat->specular);
                    prog->sendUniform("u_MatShininess", mat->shininess);
                }

                // compute modelview matrix
                glm::mat4 modelview = viewMatrix * ent->getWorldMatrix();

                // send the entity's modelview and normal matrix
                prog->sendUniform("u_ModelviewMatrix", modelview);
                prog->sendUniform("u_NormalMatrix", glm::transpose(glm::inverse(glm::mat3(modelview))));

                // use the entity's mesh
                const Mesh* mesh = ent->getMesh();
                mesh->activate();
                
            }
            else {
                Entity* ent = mEntities[i];

                // use the entity's material
                const Material* mat = ent->getMaterial();
                glBindTexture(GL_TEXTURE_2D, mat->tex->id());   // bind texture
                prog->sendUniform("u_Tint", mat->tint);     // send tint color

                // send the Blinn-Phong parameters, if required
                if (mLightingModel > PER_VERTEX_DIR_LIGHT) {
                    prog->sendUniform("u_MatEmissiveColor", mat->emissive);
                    prog->sendUniform("u_MatSpecularColor", mat->specular);
                    prog->sendUniform("u_MatShininess", mat->shininess);
                }

                // compute modelview matrix
                glm::mat4 modelview = viewMatrix * ent->getWorldMatrix();

                // send the entity's modelview and normal matrix
                prog->sendUniform("u_ModelviewMatrix", modelview);
                prog->sendUniform("u_NormalMatrix", glm::transpose(glm::inverse(glm::mat3(modelview))));

                // use the entity's mesh
                const Mesh* mesh = ent->getMesh();
                mesh->activate();
                mesh->draw();
            }
        }
        else {
            Entity* ent = mEntities[i];

            // use the entity's material
            const Material* mat = ent->getMaterial();
            glBindTexture(GL_TEXTURE_2D, mat->tex->id());   // bind texture
            prog->sendUniform("u_Tint", mat->tint);     // send tint color

            // send the Blinn-Phong parameters, if required
            if (mLightingModel > PER_VERTEX_DIR_LIGHT) {
                prog->sendUniform("u_MatEmissiveColor", mat->emissive);
                prog->sendUniform("u_MatSpecularColor", mat->specular);
                prog->sendUniform("u_MatShininess", mat->shininess);
            }

            // compute modelview matrix
            glm::mat4 modelview = viewMatrix * ent->getWorldMatrix();

            // send the entity's modelview and normal matrix
            prog->sendUniform("u_ModelviewMatrix", modelview);
            prog->sendUniform("u_NormalMatrix", glm::transpose(glm::inverse(glm::mat3(modelview))));

            // use the entity's mesh
            const Mesh* mesh = ent->getMesh();
            mesh->activate();
            mesh->draw();

        }

    }

    //
    // draw local axes for current entity
    //

    mDbgProgram->activate();
    mDbgProgram->sendUniform("u_ProjectionMatrix", mProjMatrix);

    Entity* activeEntity = mEntities[mActiveEntityIndex];
    mDbgProgram->sendUniform("u_ModelviewMatrix", viewMatrix * activeEntity->getWorldMatrix());
    mAxes->activate();
    mAxes->draw();

    //acess this entity index when you have move the specific meshes ***********************************************************************
    //Entity* activeEntity = mEntities[10];
    //mDbgProgram->sendUniform("u_ModelviewMatrix", viewMatrix* activeEntity->getWorldMatrix());

    //Entity* activeEntity = mEntities[10];
    //mDbgProgram->sendUniform("u_ModelviewMatrix", viewMatrix* activeEntity->getWorldMatrix());

    CHECK_GL_ERRORS("drawing");

}

static float t;
static float g;
static float c;
static float d;

void SplineInit()
{
	t = 0.0f;
    g = 0.0f;
    c = 0.00f;
    d = 0.00f;
}

glm::vec3 BasicSceneRenderer::SplinePointOnCurve(float dt, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
	//
	//	The spline passes through all of the control points.
	//	The spline is C1 continuous, meaning that there are no discontinuities in the tangent direction and magnitude.
	//	The spline is not C2 continuous.  The second derivative is linearly interpolated within each segment, causing the curvature to vary linearly over the length of the segment.
	//	Points on a segment may lie outside of the domain of P1 -> P2.
	glm::vec3 vOut = glm::vec3(0.0f, 0.0f, 0.0f);
    //std::cout << "________1.[[[[[[[[[[[[[[[[[[[[[[[[[[[[[T is =" << t << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
	// update state
	t += dt;
	if (t > 1.0f)
		t = 1.0f;

    //std::cout << "________[[[[[[[[[[[[[[[[[[[[[[[[[[[[[dt is =" << dt << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
    //std::cout << "________2.[[[[[[[[[[[[[[[[[[[[[[[[[[[[[T is =" << t << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
	float t2 = t * t;
	float t3 = t2 * t;

	vOut = 0.5f * ((2.0f * p1) + (-p0 + p2) * t + (2.0f * p0 - 5.0f * p1 + 4.0f * p2 - p3) * t2 + (-p0 + 3.0f * p1 - 3.0f * p2 + p3) * t3);
    //std::cout << "[[[[[[[[[[[[[[[[[[[[[[[[[[[[what is vOut when t is 1? = " << vOut.x << ", " << vOut.y << ", " << vOut.z << ") ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
	//i should do vOut == mEntities[29]->getPosition(), i should instead do vOut.x == mEntities[29]->getPosition().x) && (vOut.y == mEntities[29]->getPosition().y) && (vOut.z == mEntities[29]->getPosition().z)
    //std::cout << "[[[[[[[[[[[[[[[[[[[[[[[[[[[[what is mEntities[29]->getPosition() when t is 1? = " << mEntities[29]->getPosition().x << ", " << mEntities[29]->getPosition().y << ", " << mEntities[29]->getPosition().z << ") ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
    /*
    float x= vOut.x;
    float y = vOut.y;
    float z = vOut.z;

    float x2 = mEntities[29]->getPosition().x;
    float y2 = mEntities[29]->getPosition().y;
    float z2 = mEntities[29]->getPosition().z;
    if (x == x2) {
        std::cout << "yes to vOut.x == mEntities[29]->getPosition().x" << std::endl;
    }

    if (y == y2) {
        std::cout << "yes to vOut.y == mEntities[29]->getPosition().y" << std::endl;
    }
    if (z == z2) {
        std::cout << "yes to vOut.z == mEntities[29]->getPosition().z" << std::endl;
    }



    if (vOut.x == mEntities[29]->getPosition().x) {
        std::cout << "yes to vOut.x == mEntities[29]->getPosition().x" << std::endl;
    }

    if (vOut.y == mEntities[29]->getPosition().y) {
        std::cout << "yes to vOut.y == mEntities[29]->getPosition().y" << std::endl;
    }
    if (vOut.z == mEntities[29]->getPosition().z) {
        std::cout << "yes to vOut.z == mEntities[29]->getPosition().z" << std::endl;
    }
    

    if (t == 1.0f) {
        g = 0.0f;
        std::cout << "does it go inthis t == 1 if statement? yesss" << std::endl;

        vOut = SplinePointOnCurveA(dt, mEntities[31]->getPosition(), vOut, mEntities[28]->getPosition(), mEntities[32]->getPosition());
        std::cout << "[[[[[[[[[[[[[[[[[[[[[[[[[[[[what is vOut after it has just finished calling the = SplinePointOnCurveA() function" << vOut.x << ", " << vOut.y << ", " << vOut.z << ") ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;

        return vOut;
    }
    else {
        return vOut;
    }
    */
    if (t >= 1.0f) {
        splinesVector = vOut;
        g = 0.0f;
    }



    return vOut;
    
}

glm::vec3 BasicSceneRenderer::SplinePointOnCurveC(float dt, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
    //
    //	The spline passes through all of the control points.
    //	The spline is C1 continuous, meaning that there are no discontinuities in the tangent direction and magnitude.
    //	The spline is not C2 continuous.  The second derivative is linearly interpolated within each segment, causing the curvature to vary linearly over the length of the segment.
    //	Points on a segment may lie outside of the domain of P1 -> P2.
    glm::vec3 vOut = glm::vec3(0.0f, 0.0f, 0.0f);
    //std::cout << "________1.[[[[[[[[[[[[[[[[[[[[[[[[[[[[[T is =" << t << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
    // update state
    c += dt;
    if (c > 1.0f)
        c = 1.0f;

    //std::cout << "________[[[[[[[[[[[[[[[[[[[[[[[[[[[[[dt is =" << dt << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
    //std::cout << "________2.[[[[[[[[[[[[[[[[[[[[[[[[[[[[[T is =" << t << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
    float t2 = t * t;
    float t3 = t2 * t;

    vOut = 0.5f * ((2.0f * p1) + (-p0 + p2) * t + (2.0f * p0 - 5.0f * p1 + 4.0f * p2 - p3) * t2 + (-p0 + 3.0f * p1 - 3.0f * p2 + p3) * t3);

    if (c >= 1.0f) {
        splinesVectorC = vOut;
        d = 0.0f;
    }



    return vOut;

}



bool BasicSceneRenderer::update(float dt)
{
    const Keyboard* kb = getKeyboard();

    if (kb->keyPressed(KC_ESCAPE))
        return false;
    
    // move forward through our list of entities
    if (kb->keyPressed(KC_X)) {
        // compute next entity index
        
        
        
        ++mActiveEntityIndex;
        std::cout << "________current mActiveEntityIndex is =" << mActiveEntityIndex << " with position (" << mEntities[mActiveEntityIndex]->getPosition().x << ", " << mEntities[mActiveEntityIndex]->getPosition().y << ", " << mEntities[mActiveEntityIndex]->getPosition().z << ") " << std::endl;
        std::cout << "size of mEntities.size() = (" << mEntities.size() << ") " << std::endl;
        if (mActiveEntityIndex >= (int)mEntities.size())
            mActiveEntityIndex = 0;
            std::cout << "________current mActiveEntityIndex is =" << mActiveEntityIndex << " with position (" << mEntities[mActiveEntityIndex]->getPosition().x << ", " << mEntities[mActiveEntityIndex]->getPosition().y << ", " << mEntities[mActiveEntityIndex]->getPosition().z << ") " << std::endl;
            std::cout << "size of mEntities.size() = (" << mEntities.size() << ") " << std::endl;
    }

    // move backward through our list of entities
    if (kb->keyPressed(KC_Z)) {
        // compute previous entity index
        
        --mActiveEntityIndex;
        std::cout << "________current mActiveEntityIndex is =" << mActiveEntityIndex << " with position (" << mEntities[mActiveEntityIndex]->getPosition().x << ", " << mEntities[mActiveEntityIndex]->getPosition().y << ", " << mEntities[mActiveEntityIndex]->getPosition().z << ") " << std::endl;
        if (mActiveEntityIndex < 0)
            mActiveEntityIndex = (int)mEntities.size() - 1;
            std::cout << "________current mActiveEntityIndex is =" << mActiveEntityIndex << " with position (" << mEntities[mActiveEntityIndex]->getPosition().x << ", " << mEntities[mActiveEntityIndex]->getPosition().y << ", " << mEntities[mActiveEntityIndex]->getPosition().z << ") " << std::endl;

    }
    
    // get the entity to manipulate
    Entity* activeEntity = mEntities[mActiveEntityIndex];
    
    // rotate the entity
    float rotSpeed = 90;
    float rotAmount = rotSpeed * dt;
    if (kb->isKeyDown(KC_LEFT))
        activeEntity->rotate(rotAmount, 0, 1, 0);
    if (kb->isKeyDown(KC_RIGHT))
        activeEntity->rotate(-rotAmount, 0, 1, 0);
    if (kb->isKeyDown(KC_UP))
        activeEntity->rotate(rotAmount, 1, 0, 0);
    if (kb->isKeyDown(KC_DOWN))
        activeEntity->rotate(-rotAmount, 1, 0, 0);

    // reset entity orientation
    if (kb->keyPressed(KC_R))
        activeEntity->setOrientation(glm::quat(1.0f, 0.0f, 0.0f, 0.0f));

    float speed = 3;
    float disp = speed * dt;

    // move entity along world axes
    if (kb->isKeyDown(KC_I))
        activeEntity->translate(0, 0, disp);
    if (kb->isKeyDown(KC_K))
        activeEntity->translate(0, 0, -disp);
    if (kb->isKeyDown(KC_L))
        activeEntity->translate(disp, 0, 0);
    if (kb->isKeyDown(KC_J))
        activeEntity->translate(-disp, 0, 0);
    // Move Up and down in world space *********************************************************************************i just added these
    if (kb->isKeyDown(KC_O))
        activeEntity->translate(0, disp, 0);
    if (kb->isKeyDown(KC_P))
        activeEntity->translate(0, -disp, 0);

    // move entity along entity's local axes
    if (kb->isKeyDown(KC_T))
        activeEntity->translateLocal(0, 0, disp);
    if (kb->isKeyDown(KC_G))
        activeEntity->translateLocal(0, 0, -disp);
    if (kb->isKeyDown(KC_F))
        activeEntity->translateLocal(disp, 0, 0);
    if (kb->isKeyDown(KC_H))
        activeEntity->translateLocal(-disp, 0, 0);
    // Move Up and down in object space *******************************************************************************i just added these
    if (kb->isKeyDown(KC_Y))
        activeEntity->translateLocal(0, disp, 0);
    if (kb->isKeyDown(KC_U))
        activeEntity->translateLocal(0, -disp, 0);


    if (kb->isKeyDown(KC_Q))
        hideAllCones = !hideAllCones;
    

    //std::cout << "position of current object in world space: (" << activeEntity->getPosition().x << ", " << activeEntity->getPosition().y << ", " << activeEntity->getPosition().z << ") " << std::endl;

    //check for Collision// what happens if i just call it like that withought putting it in a variable?**************************************************************
    isCollision(mActiveEntityIndex);
   







    // change lighting models
    if (kb->keyPressed(KC_1))
        mLightingModel = PER_VERTEX_DIR_LIGHT;
    if (kb->keyPressed(KC_2))
        mLightingModel = BLINN_PHONG_PER_FRAGMENT_DIR_LIGHT;
    if (kb->keyPressed(KC_3))
        mLightingModel = BLINN_PHONG_PER_FRAGMENT_POINT_LIGHT;
    if (kb->keyPressed(KC_4))
        mLightingModel = BLINN_PHONG_PER_FRAGMENT_MULTI_LIGHT;

    // toggle visualization of point lights
    if (kb->keyPressed(KC_TAB))
        mVisualizePointLights = !mVisualizePointLights;

	//
	// start spline
	//
	static bool bSplineStart = true;
	if (kb->keyPressed(KC_S))
	{
		SplineInit();
		bSplineStart = true;
	}
	if (bSplineStart)
	{


        //it should be if t is != 1.0 rather than t< 1.0 cus t can be greater than 1.0 too
        if (t != 1.0f && useNextSplinesVector == true) {
            
            glm::vec3 vPos = SplinePointOnCurve(dt, mEntities[27]->getPosition(),
                    splinesVector2,
                    mEntities[29]->getPosition(),
                    mEntities[30]->getPosition());

            //std::cout << "In 3rd : vPos = : (" << vPos.x << ", " << vPos.y << ", " << vPos.z << ") " << std::endl;

            //std::cout << "mEntities[4]->getPosition(): (" << mEntities[4]->getPosition().x << ", " << mEntities[4]->getPosition() .y << mEntities[4]->getPosition().z << ") " << std::endl;
            mEntities[26]->setPosition(vPos);

            //mEntities[24]->translate(vPos.x, vPos.y, vPos.z);
            mEntities[24]->setPosition(vPos + glm::vec3(0, 0, (0.5f * coneMeshHeight) + 1));
            //mEntities[24]->setPosition(vPos + (mEntities[24]->getPosition() + glm::vec3(0.0f, 0.0f, 0.5f * coneMeshHeight)));
        }

        //1st
        else if (t != 1.0f) {
            glm::vec3 vPos = SplinePointOnCurve(dt, mEntities[27]->getPosition(),
                mEntities[28]->getPosition(),
                mEntities[29]->getPosition(),
                mEntities[30]->getPosition());
            //std::cout << "In 1st : vPos = : (" << vPos.x << ", " << vPos.y <<", " << vPos.z << ") " << std::endl;
            //std::cout << "mEntities[4]->getPosition(): (" << mEntities[4]->getPosition().x << ", " << mEntities[4]->getPosition() .y << mEntities[4]->getPosition().z << ") " << std::endl;
            mEntities[26]->setPosition(vPos);

            //mEntities[24]->translate(vPos.x, vPos.y, vPos.z);
            mEntities[24]->setPosition(vPos + glm::vec3(0, 0, (0.5f * coneMeshHeight) + 1));
            //mEntities[24]->setPosition(vPos + (mEntities[24]->getPosition() + glm::vec3(0.0f, 0.0f, 0.5f * coneMeshHeight)));
        }
        //2nd
        else {
      
            glm::vec3 a = glm::vec3(0, 0, -(mEntities[30]->getPosition().z));
            glm::vec3 b = glm::vec3(0, 0, -(mEntities[27]->getPosition().z));
            //std::cout << "what is splinesVector befor going into SplinePointOnCurveA() function: (" << splinesVector.x << ", " << splinesVector.y << ", " << splinesVector.z << ") " << std::endl;
            /*
            std::cout << "p0/a befor going into SplinePointOnCurveA() function (" << a.x << ", " << a.y << ", " << a.z << ") " << std::endl;
            std::cout << "[what really matters]p1/splinesVector befor going into SplinePointOnCurveA() function (" << splinesVector.x << ", " << splinesVector.y << ", " << splinesVector.z << ") " << std::endl;
            std::cout << "p2/mEntities[28]->getPosition() befor going into SplinePointOnCurveA() function (" << mEntities[28]->getPosition().x << ", " << mEntities[28]->getPosition().y << ", " << mEntities[28]->getPosition().z << ") " << std::endl;
            std::cout << "p3/b gotten befor going into SplinePointOnCurveA() function (" << b.x << ", " << b.y << ", " << b.z << ") " << std::endl;
            */
            
            
            //glm::vec3 vPos = SplinePointOnCurveA(dt, a, splinesVector, mEntities[28]->getPosition(), b);
            
            glm::vec3 vPos = SplinePointOnCurveA(dt, mEntities[31]->getPosition(), splinesVector, mEntities[28]->getPosition(), mEntities[32]->getPosition());
            //glm::vec3 vPos = SplinePointOnCurveA(dt, glm::vec3(mEntities[30]->getPosition().x, mEntities[30]->getPosition().y, -(mEntities[30]->getPosition().z)), mEntities[29]->getPosition(), mEntities[28]->getPosition(), glm::vec3(mEntities[27]->getPosition().x, mEntities[27]->getPosition().y, -mEntities[27]->getPosition().z));
            
            //std::cout << "In 2nd : vPos = : (" << vPos.x << ", " << vPos.y << ", "<< vPos.z << ") " << std::endl;
         
            mEntities[26]->setPosition(vPos);

            mEntities[24]->setPosition(vPos + glm::vec3(0, 0, (0.5f * coneMeshHeight) + 1));
      
            
        }



        //3rd
        if (c != 1.0f && useNextSplinesVectorD == true) {
            glm::vec3 vPos = SplinePointOnCurveC(dt, mEntities[39]->getPosition(),
                mEntities[38]->getPosition(),
                mEntities[35]->getPosition(),
                mEntities[37]->getPosition());

            //std::cout << "In 3rd : vPos = : (" << vPos.x << ", " << vPos.y << ", " << vPos.z << ") " << std::endl;

            //std::cout << "mEntities[4]->getPosition(): (" << mEntities[4]->getPosition().x << ", " << mEntities[4]->getPosition() .y << mEntities[4]->getPosition().z << ") " << std::endl;
            mEntities[42]->setPosition(vPos);

            //mEntities[24]->translate(vPos.x, vPos.y, vPos.z);
            mEntities[40]->setPosition(vPos + glm::vec3(0, 0, (0.5f * coneMeshHeight) + 1));
            //mEntities[24]->setPosition(vPos + (mEntities[24]->getPosition() + glm::vec3(0.0f, 0.0f, 0.5f * coneMeshHeight)));
        }
        //1st
        else if (c != 1.0f) {
            glm::vec3 vPos = SplinePointOnCurveC(dt, mEntities[33]->getPosition(),
                mEntities[34]->getPosition(),
                mEntities[35]->getPosition(),
                mEntities[36]->getPosition());
            //std::cout << "In 1st : vPos = : (" << vPos.x << ", " << vPos.y <<", " << vPos.z << ") " << std::endl;
            //std::cout << "mEntities[4]->getPosition(): (" << mEntities[4]->getPosition().x << ", " << mEntities[4]->getPosition() .y << mEntities[4]->getPosition().z << ") " << std::endl;
            mEntities[42]->setPosition(vPos);

            //mEntities[24]->translate(vPos.x, vPos.y, vPos.z);
            mEntities[40]->setPosition(vPos + glm::vec3(0, 0, (0.5f * coneMeshHeight) + 1));
            //mEntities[24]->setPosition(vPos + (mEntities[24]->getPosition() + glm::vec3(0.0f, 0.0f, 0.5f * coneMeshHeight)));
        }
        //2nd
        else {
            glm::vec3 a = glm::vec3(0, 0, -(mEntities[30]->getPosition().z));
            glm::vec3 b = glm::vec3(0, 0, -(mEntities[27]->getPosition().z));
            //std::cout << "what is splinesVector befor going into SplinePointOnCurveA() function: (" << splinesVector.x << ", " << splinesVector.y << ", " << splinesVector.z << ") " << std::endl;
            /*
            std::cout << "p0/a befor going into SplinePointOnCurveA() function (" << a.x << ", " << a.y << ", " << a.z << ") " << std::endl;
            std::cout << "[what really matters]p1/splinesVector befor going into SplinePointOnCurveA() function (" << splinesVector.x << ", " << splinesVector.y << ", " << splinesVector.z << ") " << std::endl;
            std::cout << "p2/mEntities[28]->getPosition() befor going into SplinePointOnCurveA() function (" << mEntities[28]->getPosition().x << ", " << mEntities[28]->getPosition().y << ", " << mEntities[28]->getPosition().z << ") " << std::endl;
            std::cout << "p3/b gotten befor going into SplinePointOnCurveA() function (" << b.x << ", " << b.y << ", " << b.z << ") " << std::endl;
            */


            //glm::vec3 vPos = SplinePointOnCurveA(dt, a, splinesVector, mEntities[28]->getPosition(), b);
            //i changed the spline variable to make it more literal**********************************************************************
            glm::vec3 vPos = SplinePointOnCurveD(dt, mEntities[37]->getPosition(), mEntities[35]->getPosition(), mEntities[38]->getPosition(), mEntities[39]->getPosition());
            //glm::vec3 vPos = SplinePointOnCurveA(dt, glm::vec3(mEntities[30]->getPosition().x, mEntities[30]->getPosition().y, -(mEntities[30]->getPosition().z)), mEntities[29]->getPosition(), mEntities[28]->getPosition(), glm::vec3(mEntities[27]->getPosition().x, mEntities[27]->getPosition().y, -mEntities[27]->getPosition().z));

            //std::cout << "In 2nd : vPos = : (" << vPos.x << ", " << vPos.y << ", "<< vPos.z << ") " << std::endl;

            mEntities[42]->setPosition(vPos);

            mEntities[40]->setPosition(vPos + glm::vec3(0, 0, (0.5f * coneMeshHeight) + 1));
        }


       

	}

    // update the camera
    mCamera->update(dt);

    return true;
}




glm::vec3 BasicSceneRenderer::SplinePointOnCurveA(float dt, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
    //
    //	The spline passes through all of the control points.
    //	The spline is C1 continuous, meaning that there are no discontinuities in the tangent direction and magnitude.
    //	The spline is not C2 continuous.  The second derivative is linearly interpolated within each segment, causing the curvature to vary linearly over the length of the segment.
    //	Points on a segment may lie outside of the domain of P1 -> P2.
    /*
    std::cout << "p0 gotten from previous splines call is (" << p0.x << ", " << p0.y << ", " << p0.z << ") " << std::endl;
    std::cout << "p1 gotten from previous splines call is (" << p1.x <<", " << p1.y << ", " << p1.z <<") "<< std::endl;
    std::cout << "p2 gotten from previous splines call is (" << p2.x << ", " << p2.y << ", " << p2.z << ") " << std::endl;
    std::cout << "p3 gotten from previous splines call is (" << p3.x << ", " << p3.y << ", " << p3.z << ") " << std::endl;
    */


    glm::vec3 vOut = glm::vec3(0.0f, 0.0f, 0.0f);

    // update state
    g += dt;
    if (g > 1.0f)
        g = 1.0f;
    float t2 = g * g;
    float t3 = t2 * g;

    //std::cout << "________[[[[[[[[[[[[[[[[[[[[[[[[[[[[[dt in SplinePointOnCurveA is =" << dt << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
    //std::cout << "________2.[[[[[[[[[[[[[[[[[[[[[[[[[[[[[T in SplinePointOnCurveA is =" << g << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;

    vOut = 0.5f * ((2.0f * p1) + (-p0 + p2) * t + (2.0f * p0 - 5.0f * p1 + 4.0f * p2 - p3) * t2 + (-p0 + 3.0f * p1 - 3.0f * p2 + p3) * t3);
    
    if(g >= 1.0f) {
        splinesVector2 = vOut;
        useNextSplinesVector = true;
        t = 0.0f;
        //maybe i need to change this to static?
        

    }
    


    

    return vOut;



}

glm::vec3 BasicSceneRenderer::SplinePointOnCurveD(float dt, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3)
{
    //
    //	The spline passes through all of the control points.
    //	The spline is C1 continuous, meaning that there are no discontinuities in the tangent direction and magnitude.
    //	The spline is not C2 continuous.  The second derivative is linearly interpolated within each segment, causing the curvature to vary linearly over the length of the segment.
    //	Points on a segment may lie outside of the domain of P1 -> P2.

    /*
    std::cout << "p0 gotten from previous splines call is (" << p0.x << ", " << p0.y << ", " << p0.z << ") " << std::endl;
    std::cout << "p1 gotten from previous splines call is (" << p1.x << ", " << p1.y << ", " << p1.z << ") " << std::endl;
    std::cout << "p2 gotten from previous splines call is (" << p2.x << ", " << p2.y << ", " << p2.z << ") " << std::endl;
    std::cout << "p3 gotten from previous splines call is (" << p3.x << ", " << p3.y << ", " << p3.z << ") " << std::endl;
    */


    glm::vec3 vOut = glm::vec3(0.0f, 0.0f, 0.0f);

    // update state
    d += dt;
    if (d > 1.0f)
        d = 1.0f;
    float t2 = d * d;
    float t3 = t2 * d;

    //std::cout << "________[[[[[[[[[[[[[[[[[[[[[[[[[[[[[dt in SplinePointOnCurveA is =" << dt << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;
    //std::cout << "________2.[[[[[[[[[[[[[[[[[[[[[[[[[[[[[T in SplinePointOnCurveA is =" << d << "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]" << std::endl;

    vOut = 0.5f * ((2.0f * p1) + (-p0 + p2) * t + (2.0f * p0 - 5.0f * p1 + 4.0f * p2 - p3) * t2 + (-p0 + 3.0f * p1 - 3.0f * p2 + p3) * t3);

    if (g >= 1.0f) {
        splinesVectorD = vOut;
        useNextSplinesVectorD = true;
        c = 0.0f;
        //maybe i need to change this to static?


    }





    return vOut;



}

// so it seems that u can take structs or other user defined classes as a  parameter only if you are apart of the class in the header file h.
void BasicSceneRenderer::ClosestPtPoint(glm::vec3* p, Obb* b, glm::vec3* q) {

    //std::cout << "1.passthroughpoint : (" << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") " << std::endl;

    //std::cout << "                q is: (" << closest.x << ", " << closest.y << ", " << closest.z << ") " << std::endl;
    glm::vec3 bb = glm::vec3(obb.c.x,obb.c.y,obb.c.z);
    
    //std::cout << "___ 1.____ bb(A.K.A The box's position) is: (" << bb.x << ", " << bb.y << ", " << bb.z << ") " << std::endl;
    
    glm::vec3 jj = glm::vec3(passThroughPointToCubeSpace.x, passThroughPointToCubeSpace.y, passThroughPointToCubeSpace.z);
    //std::cout << "___ jj/passThroughPointToCubeSpace is: (" << jj.x << ", " << jj.y << ", " << jj.z << ") " << std::endl;
    //changed to ->// * besides a pointer tells it to bring forth the value that the pointer holds
    //glm::vec3 d = jj - b.c;
    glm::vec3 d = jj - bb;
    //std::cout << "___ d = jj - bb = (" << d.x << ", " << d.y << ", " << d.z << ") " << std::endl;

    //q is the vector to return
    //q = b.c;
    closest = bb;
    //std::cout << "inital closest value: (" << closest.x << ", " << closest.y << ", " << closest.z << ")" << std::endl;
    int l = 0;
    

    for (int i = 0; i < 3; i++) {
        //std::cout << "_______________ current unit variable, obb.u[i]= : (" << obb.u[i].x << ", " << obb.u[i].y << ", " << obb.u[i].z << ") " << std::endl;

        float dist;
        if (l == 0) {
            //i just want to know the difference thus i need tosquare it(obb.u[i].z) the square root it
            //dist = sqrt(pow(d.z, 2)) - obb.u[i].z;
            //dist = d.z - obb.u[i].z;
            //^
            dist = d.z;
            //std::cout << "_______________ current unit variable z value obb.u[i].z: (" << obb.u[i].z << ")" << std::endl;
        }
        if (l == 1) {
            //dist = sqrt(pow(d.y, 2)) - obb.u[i].y;
            //dist = d.y - obb.u[i].y;
            //^
            dist = d.y;
            //std::cout << "_______________ current unit variable y value obb.u[i].y: (" << obb.u[i].y << ")" << std::endl;
        }
        if (l == 2) {
            //dist = sqrt(pow(d.x, 2)) - obb.u[i].x;
            //dist = d.x - obb.u[i].x;
            //^
            dist = d.x;
            //std::cout << "_______________ current unit variable x value obb.u[i].x: (" << obb.u[i].x << ")" << std::endl;
        }
        //std::cout << "obb.u[" << l << "] =(" << obb.u[i].x << ", " << obb.u[i].y << ", " << obb.u[i].z << ") " << std::endl;
        //temporarily comment out
        //float dist = sqrt(pow(d.x - obb.u[i].x, 2) + pow(d.y - obb.u[i].y, 2) + pow(d.z - obb.u[i].z, 2));
        
        //  wanna know the 2 vectors the came to give me this answer**************************
        //std::cout << "dist: for obb.u["<< l << "] to founded difference vector" << dist << std::endl;
        // DONT FORGET UR ELSE IF STATEMENTS
     
        if (l == 0 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;
            
        }
        else if (l == 0 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if(l == 0 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }



        /*
        if (l == 1 && dist < -obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 1 && dist > obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "] so now its" << dist << std::endl;
        }
        else if (l == 1 && dist == -1.0f) {
            dist = 0.0f;
            std::cout << "dist is -1.0f so so now its " << dist << std::endl;
        }
        */
        if (l == 1 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;

        }
        else if (l == 1 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 1 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }





        /*
        if (l == 2 && dist < -obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 2 && dist > obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "] so now its" << dist << std::endl;
        }
        else if (l == 2 && dist == -1.0f) {
            dist = 0.0f;
            std::cout << "dist is -1.0f so so now its " << dist << std::endl;
        }
        */

        if (l == 2 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;

        }
        else if (l == 2 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 2 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }




        /*

        if (dist > obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "]" << dist << std::endl;
        }
        else if (dist < -obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is less than for obb.e[" << l << "]" << dist << std::endl;
        }
        else if (dist == -1.0f) {
            dist = 0.0f;
        }
        else if (dist == 1.0f) {

        }

        */

        closest += (dist * obb.u[i]);
        //std::cout << "current iteration of closest value for iteration with obb.u[" << l << "] is : (" << closest.x << ", " << closest.y << ", " << closest.z << ")" << std::endl;
        l++;
    }
    

}


void BasicSceneRenderer::ClosestPtPoint2(glm::vec3* p, Obb* b, glm::vec3* q) {

    //std::cout << "1.passthroughpoint : (" << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") " << std::endl;

    //std::cout << "                q is: (" << closest.x << ", " << closest.y << ", " << closest.z << ") " << std::endl;
    glm::vec3 bb = glm::vec3(obb.c.x, obb.c.y, obb.c.z);

    //std::cout << "___ 1.____ bb(A.K.A The box's position) is: (" << bb.x << ", " << bb.y << ", " << bb.z << ") " << std::endl;

    glm::vec3 jj = glm::vec3(passThroughPointToCubeSpace2.x, passThroughPointToCubeSpace2.y, passThroughPointToCubeSpace2.z);
    //std::cout << "___ jj/passThroughPointToCubeSpace is: (" << jj.x << ", " << jj.y << ", " << jj.z << ") " << std::endl;
    //changed to ->// * besides a pointer tells it to bring forth the value that the pointer holds
    //glm::vec3 d = jj - b.c;
    glm::vec3 d = jj - bb;
    //std::cout << "___ d = jj - bb = (" << d.x << ", " << d.y << ", " << d.z << ") " << std::endl;

    //q is the vector to return
    //q = b.c;
    closest2 = bb;
    //std::cout << "inital closest value: (" << closest.x << ", " << closest.y << ", " << closest.z << ")" << std::endl;
    int l = 0;


    for (int i = 0; i < 3; i++) {
        //std::cout << "_______________ current unit variable, obb.u[i]= : (" << obb.u[i].x << ", " << obb.u[i].y << ", " << obb.u[i].z << ") " << std::endl;

        float dist;
        if (l == 0) {
            //i just want to know the difference thus i need tosquare it(obb.u[i].z) the square root it
            //dist = sqrt(pow(d.z, 2)) - obb.u[i].z;
            //dist = d.z - obb.u[i].z;
            //^
            dist = d.z;
            //std::cout << "_______________ current unit variable z value obb.u[i].z: (" << obb.u[i].z << ")" << std::endl;
        }
        if (l == 1) {
            //dist = sqrt(pow(d.y, 2)) - obb.u[i].y;
            //dist = d.y - obb.u[i].y;
            //^
            dist = d.y;
            //std::cout << "_______________ current unit variable y value obb.u[i].y: (" << obb.u[i].y << ")" << std::endl;
        }
        if (l == 2) {
            //dist = sqrt(pow(d.x, 2)) - obb.u[i].x;
            //dist = d.x - obb.u[i].x;
            //^
            dist = d.x;
            //std::cout << "_______________ current unit variable x value obb.u[i].x: (" << obb.u[i].x << ")" << std::endl;
        }
        //std::cout << "obb.u[" << l << "] =(" << obb.u[i].x << ", " << obb.u[i].y << ", " << obb.u[i].z << ") " << std::endl;
        //temporarily comment out
        //float dist = sqrt(pow(d.x - obb.u[i].x, 2) + pow(d.y - obb.u[i].y, 2) + pow(d.z - obb.u[i].z, 2));

        //  wanna know the 2 vectors the came to give me this answer**************************
        //std::cout << "dist: for obb.u["<< l << "] to founded difference vector" << dist << std::endl;
        // DONT FORGET UR ELSE IF STATEMENTS

        if (l == 0 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;

        }
        else if (l == 0 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 0 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }



        /*
        if (l == 1 && dist < -obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 1 && dist > obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "] so now its" << dist << std::endl;
        }
        else if (l == 1 && dist == -1.0f) {
            dist = 0.0f;
            std::cout << "dist is -1.0f so so now its " << dist << std::endl;
        }
        */
        if (l == 1 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;

        }
        else if (l == 1 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 1 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }





        /*
        if (l == 2 && dist < -obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 2 && dist > obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "] so now its" << dist << std::endl;
        }
        else if (l == 2 && dist == -1.0f) {
            dist = 0.0f;
            std::cout << "dist is -1.0f so so now its " << dist << std::endl;
        }
        */

        if (l == 2 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;

        }
        else if (l == 2 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 2 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }




        /*

        if (dist > obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "]" << dist << std::endl;
        }
        else if (dist < -obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is less than for obb.e[" << l << "]" << dist << std::endl;
        }
        else if (dist == -1.0f) {
            dist = 0.0f;
        }
        else if (dist == 1.0f) {

        }

        */

        closest2 += (dist * obb.u[i]);
        //std::cout << "current iteration of closest value for iteration with obb.u[" << l << "] is : (" << closest.x << ", " << closest.y << ", " << closest.z << ")" << std::endl;
        l++;
    }


}


void BasicSceneRenderer::ClosestPtPoint3(glm::vec3* p, Obb* b, glm::vec3* q) {

    //std::cout << "1.passthroughpoint : (" << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") " << std::endl;

    //std::cout << "                q is: (" << closest.x << ", " << closest.y << ", " << closest.z << ") " << std::endl;
    glm::vec3 bb = glm::vec3(obb.c.x, obb.c.y, obb.c.z);

    //std::cout << "___ 1.____ bb(A.K.A The box's position) is: (" << bb.x << ", " << bb.y << ", " << bb.z << ") " << std::endl;

    glm::vec3 jj = glm::vec3(passThroughPointToCubeSpace3.x, passThroughPointToCubeSpace3.y, passThroughPointToCubeSpace3.z);
    //std::cout << "___ jj/passThroughPointToCubeSpace is: (" << jj.x << ", " << jj.y << ", " << jj.z << ") " << std::endl;
    //changed to ->// * besides a pointer tells it to bring forth the value that the pointer holds
    //glm::vec3 d = jj - b.c;
    glm::vec3 d = jj - bb;
    //std::cout << "___ d = jj - bb = (" << d.x << ", " << d.y << ", " << d.z << ") " << std::endl;

    //q is the vector to return
    //q = b.c;
    closest3 = bb;
    //std::cout << "inital closest value: (" << closest.x << ", " << closest.y << ", " << closest.z << ")" << std::endl;
    int l = 0;


    for (int i = 0; i < 3; i++) {
        //std::cout << "_______________ current unit variable, obb.u[i]= : (" << obb.u[i].x << ", " << obb.u[i].y << ", " << obb.u[i].z << ") " << std::endl;

        float dist;
        if (l == 0) {
            //i just want to know the difference thus i need tosquare it(obb.u[i].z) the square root it
            //dist = sqrt(pow(d.z, 2)) - obb.u[i].z;
            //dist = d.z - obb.u[i].z;
            //^
            dist = d.z;
            //std::cout << "_______________ current unit variable z value obb.u[i].z: (" << obb.u[i].z << ")" << std::endl;
        }
        if (l == 1) {
            //dist = sqrt(pow(d.y, 2)) - obb.u[i].y;
            //dist = d.y - obb.u[i].y;
            //^
            dist = d.y;
            //std::cout << "_______________ current unit variable y value obb.u[i].y: (" << obb.u[i].y << ")" << std::endl;
        }
        if (l == 2) {
            //dist = sqrt(pow(d.x, 2)) - obb.u[i].x;
            //dist = d.x - obb.u[i].x;
            //^
            dist = d.x;
            //std::cout << "_______________ current unit variable x value obb.u[i].x: (" << obb.u[i].x << ")" << std::endl;
        }
        //std::cout << "obb.u[" << l << "] =(" << obb.u[i].x << ", " << obb.u[i].y << ", " << obb.u[i].z << ") " << std::endl;
        //temporarily comment out
        //float dist = sqrt(pow(d.x - obb.u[i].x, 2) + pow(d.y - obb.u[i].y, 2) + pow(d.z - obb.u[i].z, 2));

        //  wanna know the 2 vectors the came to give me this answer**************************
        //std::cout << "dist: for obb.u["<< l << "] to founded difference vector" << dist << std::endl;
        // DONT FORGET UR ELSE IF STATEMENTS

        if (l == 0 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;

        }
        else if (l == 0 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 0 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }



        /*
        if (l == 1 && dist < -obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 1 && dist > obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "] so now its" << dist << std::endl;
        }
        else if (l == 1 && dist == -1.0f) {
            dist = 0.0f;
            std::cout << "dist is -1.0f so so now its " << dist << std::endl;
        }
        */
        if (l == 1 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;

        }
        else if (l == 1 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 1 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }





        /*
        if (l == 2 && dist < -obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 2 && dist > obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "] so now its" << dist << std::endl;
        }
        else if (l == 2 && dist == -1.0f) {
            dist = 0.0f;
            std::cout << "dist is -1.0f so so now its " << dist << std::endl;
        }
        */

        if (l == 2 && dist < -obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = -obb.e[i];
            //std::cout << "________________[]dist is smaller than for -obb.e[" << l << "] so now its " << dist << std::endl;

        }
        else if (l == 2 && dist > obb.e[i]) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = obb.e[i];
            //std::cout << "________________[]dist is geater than for obb.e[" << l << "] so now its " << dist << std::endl;
        }
        else if (l == 2 && ((dist == -1.0f) || (dist == 1.0f))) {
            //std::cout << "________________[] original dist in the current axis value range: " << dist << std::endl;
            dist = 0.0f;
            //std::cout << "________________[]dist is -1.0f so so now its " << dist << std::endl;
        }




        /*

        if (dist > obb.e[i]) {
            dist = obb.e[i];
            std::cout << "dist is geater than for obb.e[" << l << "]" << dist << std::endl;
        }
        else if (dist < -obb.e[i]) {
            dist = -obb.e[i];
            std::cout << "dist is less than for obb.e[" << l << "]" << dist << std::endl;
        }
        else if (dist == -1.0f) {
            dist = 0.0f;
        }
        else if (dist == 1.0f) {

        }

        */

        closest3 += (dist * obb.u[i]);
        //std::cout << "current iteration of closest value for iteration with obb.u[" << l << "] is : (" << closest.x << ", " << closest.y << ", " << closest.z << ")" << std::endl;
        l++;
    }


}



//This should be in another class but il do that latter
void BasicSceneRenderer::isCollision(int mActiveEntityIndex) {

    // get the entity to manipulate
    Entity* activeEntity = mEntities[mActiveEntityIndex];
    // i think this type of copy was a shallow copy cus mEntities is not a pointer and it was set to another variable
    Entity* boxEntity = mEntities[25];

    // how a struct is is initialiized *************************************************************
    /*
    glm::vec3 addressXOfBoxPosition = boxEntity->getPosition();
    float addressYOfBoxPosition = boxEntity->getPosition().y;
    float addressZOfBoxPosition = boxEntity->getPosition().z;
    */
    //get position returns a constant so i converted it to vect withough const// be careful that the object being referred to does not go out of scope. So it is not legal to return a reference to local var
    //using boxEntity here is what made the value not change during game which is bad*********************************************************************************************
    //obb.c = glm::vec4(mEntities[11]->getPosition().x, mEntities[11]->getPosition().y, mEntities[11]->getPosition().z, 1);
    obb.c.x = mEntities[25]->getPosition().x;    
    obb.c.y = mEntities[25]->getPosition().y;
    obb.c.z = mEntities[25]->getPosition().z;

    //check if i can do (*(obb.c).x ***********************************************************************
    /*
    std::cout << "position of box in world space: (" << (obb.c).x << ", " << (obb.c).y << ", " << (obb.c).z << ") " << std::endl;
    std::cout << "position of current object in world space: (" << mEntities[mActiveEntityIndex]->getPosition().x << ", " << mEntities[mActiveEntityIndex]->getPosition().y << ", " << activeEntity->getPosition().z << ") " << std::endl;
    */
    // compute camera local axes //instead of orientation, il use the matrix 
    //obb.u[0] = mEntities[25]->getOrientation() * glm::vec3(0.0f, 0.0f, 1.0f);
    //MIGHT NEED TO CHANGE THE LAST PARAMETER TO 1 SO THAT TRANSFORMATIONS AFFECT THE VECTOR---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    obb.u[0] = glm::vec3(mEntities[25]->getWorldMatrix() * glm::vec4(0.0f, 0.0f, 1.0f, 0.0f));            //get the unit vector z axes vdirections by multiplying by the quaternion matrix.multiplication order is important!!!!!!
    //DONT FORGET TO REFERENCE THE CORRECT POSITION IN THE ARRAY OR UR ANSWER WILL BE WRONGE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    obb.e[0] = playerCubeMeshWidth / 2.0f;
    
    //obb.u[1] = mEntities[25]->getOrientation() * glm::vec3(0.0f, 1.0f, 0.0f);            //get the unit vector y axes vdirections by multiplying by the quaternion matrix.multiplication order is important!!!!!!
    //MIGHT NEED TO CHANGE THE LAST PARAMETER TO 1 SO THAT TRANSFORMATIONS AFFECT THE VECTOR---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    obb.u[1] = glm::vec3(mEntities[25]->getWorldMatrix() * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f));
    obb.e[1] = playerCubeMeshWidth / 2.0f;

    //obb.u[2] = mEntities[25]->getOrientation() * glm::vec3(1.0f, 0.0f, 0.0f);            //get the unit vector x axes directions by multiplying by the quaternion matrix.multiplication order is important!!!!!!
    //MIGHT NEED TO CHANGE THE LAST PARAMETER TO 1 SO THAT TRANSFORMATIONS AFFECT THE VECTOR---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    obb.u[2] = glm::vec3(mEntities[25]->getWorldMatrix() * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f));
    obb.e[2] = playerCubeMeshWidth / 2.0f;
    // maybe its cus im giving them a reference to a local vaiable and that is not allowed
    //glm::vec3 closest;
    //give cone point//mEntities[] is not a pointer unless u store it in a variable as one so change this;
    //when u acess a methood in a pointer object make sure that but the brackets "()"
    //std::cout << "object space unit vector of of is: (" << (obb.u[2]).x << ", " << (obb.u[2]).y << ", " << (obb.u[2]).z << ") " << std::endl;
    //I NEED TO MULTIPLY TOTAL VECTOR BY THE CONE'S MATRIX cus cone will change position--------------------------------------------------------------------------------------------------------------------------------------------------------
    //positionOfCone = mEntities[24]->getPosition() - glm::vec3(0, 0.5f * coneMeshHeight, 0);
    //^
    glm::mat4 mRCone = glm::toMat4(mEntities[24]->getOrientation());
    glm::mat4 mRBox = glm::toMat4(mEntities[25]->getOrientation());
    //i should say subtract by the down unit vector of the position vector so as to be dynamic and have it adjust to which ever way the object rotates, i need w of vec4 to be 1 for this
    //glm::vec3 converterVector = mEntities[24]->getPosition() - glm::vec3(0, 0.5f * coneMeshHeight, 0);

    // This is really the position
    //glm::vec3 converterVector = mEntities[24]->getPosition() - glm::vec3(mEntities[24]->getOrientation() * ((-1.0f) * glm::vec4(0, 0.5f * coneMeshHeight, 0, 1.0f)));
    glm::vec3 converterVector = (mEntities[24]->getPosition() + glm::vec3(0.0f, 0.0f, 0.5f * coneMeshHeight));

    glm::vec3 converterVector2 = (mEntities[40]->getPosition() + glm::vec3(0.0f, 0.0f, 0.5f * coneMeshHeight));
    glm::vec3 converterVector3 = (mEntities[41]->getPosition() + glm::vec3(0.0f, 0.0f, 0.5f * coneMeshHeight));


    //std::cout << "mEntities[24]->getPosition() SHOULD ALWAYS BE THESAME N MATTER WHAT: " << mEntities[24]->getPosition().x << ", " << mEntities[24]->getPosition().y << ", " << mEntities[24]->getPosition().z << ") " << std::endl;


    //glm::vec3 converterVector = glm::vec3(mEntities[24]->getWorldMatrix() * glm::vec4(0,0,0,1))  - glm::vec3(mEntities[24]->getWorldMatrix() * glm::vec4(0, 0.5f * coneMeshHeight, 0, 1.0f));
    //0,-3.5,0 for getposition() gives (0,0,13) cus it global vector rn       if i translated the right side by "glm::vec3(mEntities[24]->getWorldMatrix() * glm::vec4(0, 0.5f * coneMeshHeight, 0, 1.0f)"
    //                                 then i would get (0,-3.5,0). n answer would be 0,3.5,13
    /*
    std::cout << "converterVector is currently: " << converterVector.x << ", " << converterVector.y << ", " << converterVector.z << ") " << std::endl;
    //ALL THE 0 FOR W OF VEC4() LAST PARAMETER MEANS IS THAT THE VECTOR IS NOT AFFECTED BY TRANSLATION
    //i need to change mEntities[24]->getWorldMatrix() to orientation ****************************************************************************************
    positionOfCone = glm::vec3(mEntities[24]->getOrientation() * (glm::vec4(converterVector.x, converterVector.y, converterVector.z, 0.0f)));  //unsureeeeeeeeeeeeeeeee about the 0
    std::cout << "1st positionOfCone is currently: " << positionOfCone.x << ", " << positionOfCone.y << ", " << positionOfCone.z << ") " << std::endl;
    //checking this out// im bringing the position back to world space then bringing it to the other object space-------------------------------------------------|||||||||||||||||||-----------------------------------------------------------------
    positionOfCone = glm::vec3(glm::mat4(1.0f) * glm::vec4(positionOfCone.x, positionOfCone.y, positionOfCone.z, 0.0f));                       //unsure about if i need the inbetween now
    std::cout << "2nd positionOfCone is currently: " << positionOfCone.x << ", " << positionOfCone.y << ", " << positionOfCone.z << ") " << std::endl;
    //^
    //positionOfCone = mEntities[24]->getPosition();
    positionOfConeAsVectorExplicitly = glm::vec4(positionOfCone.x, positionOfCone.y, positionOfCone.z, 0.0f);//********************* it might jdt b btt f  mak ths 0 nt a 1, //unsureeeeeeeeeeeeeeeee about the 0
    std::cout << "positionOfConeAsVectorExplicitly is currently: " << positionOfConeAsVectorExplicitly.x << ", " << positionOfConeAsVectorExplicitly.y << ", " << positionOfConeAsVectorExplicitly .z << ") " << std::endl;
    //passThroughPointToCubeSpace = mEntities[25]->getWorldMatrix() * positionOfConeAsVectorExplicitly;
    //i need to chnage mEntities[25]->getWorldMatrix() to orientation **********************************************************************************************
    passThroughPointToCubeSpace = glm::vec3(glm::mat4(1.0f) * (mEntities[25]->getOrientation() * positionOfConeAsVectorExplicitly));
    //passThroughPointToCubeSpace = glm::vec3((mRBox * positionOfConeAsVectorExplicitly));
    */


    //were gonna try on ly orientation 

    /*

    //bring it into world space[inverse & w=0.0f]//if already there it will return thesame value as if nothing changed
    glm::vec3 hold = glm::vec3(glm::inverse(mEntities[24]->getWorldMatrix()) * (glm::vec4(converterVector.x, converterVector.y, converterVector.z, 0.0f)));
    glm::vec3 hold2 = glm::vec3(glm::inverse(mEntities[24]->getWorldMatrix()) * (glm::vec4(hold.x, hold.y, hold.z, 0.0f)));
    //IM NOT TRYING TO CHANGE THE POINT WHEN I BRING IT INTO ANOTHER SPACE SO IL PUT 0.0F. 
    //((((((((((((((((((((((((((((((((((((((((((((((((((((((((View 0 as what would this be in this next space withought changing the point, and view 1 as what would the version of this be in this new space))))))))))))))))))
    //start at zero of ur own object space and replicatfrom this vector movement then give me the direction vector from the orgin of the world space to the new location that u put it.[matrix & w=1.0f]          
    passThroughPointToCubeSpace = glm::vec3((mEntities[25]->getWorldMatrix() * glm::vec4(hold.x, hold.y, hold.z, 1.0f)));
    //std::cout << "1st official passThroughPointToCubeSpace is currently: " << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") " << std::endl;
    //take the direction vector from the orgin of the world space to the new location that u put it, and give me the real world space value[inverse & w=1.0f]
    glm::vec3 outOfpassThroughPointToCubeSpace = glm::vec3(glm::inverse(mEntities[25]->getWorldMatrix()) * (glm::vec4(passThroughPointToCubeSpace.x, passThroughPointToCubeSpace.y, passThroughPointToCubeSpace.z, 1.0f)));
    //passThroughPointToCubeSpace = glm::vec3((mEntities[25]->getWorldMatrix() * glm::vec4(outOfpassThroughPointToCubeSpace.x, outOfpassThroughPointToCubeSpace.y, outOfpassThroughPointToCubeSpace.z, 1.0f)));
    //glm::vec3 kj = glm::vec3(glm::inverse(glm::mat3(mEntities[25]->getWorldMatrix())) * (glm::vec4(passThroughPointToCubeSpace.x, passThroughPointToCubeSpace.y, passThroughPointToCubeSpace.z, 1.0f)));

    std::cout << "Lets see if this brings it into global space: " << hold.x << ", " << hold.y << ", " << hold.z << ") " << std::endl;
    std::cout << "hold2 Lets see if this brings it into global space again: " << hold2.x << ", " << hold2.y << ", " << hold2.z << ") " << std::endl;
    std::cout << "___2nd official passThroughPointToCubeSpace is currently: " << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") " << std::endl;
    std::cout << "outOfpassThroughPointToCubeSpace 1st change to global space is currently: " << outOfpassThroughPointToCubeSpace.x << ", " << outOfpassThroughPointToCubeSpace.y << ", " << outOfpassThroughPointToCubeSpace.z << ") " << std::endl;
    

    */
    passThroughPointToCubeSpace = converterVector;
    passThroughPointToCubeSpace2 = converterVector2;
    passThroughPointToCubeSpace3 = converterVector3;

    //passThroughPointToCubeSpace = converterVector;
    // is it surposed to be put in object space?????????????????***************************
    //the box's position should not move localy so dont press the keys that do that rn and latter i need to remove it
    //[LAST CASE SENARIO] THIS MIGHT NEED TO BE MULTIPLIED BY A VECTOR BY IT'S BOX'S ORIENTATION---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    closest = mEntities[25]->getPosition();
    closest2 = mEntities[25]->getPosition();
    closest3 = mEntities[25]->getPosition();

    ClosestPtPoint(&passThroughPointToCubeSpace, &obb, &closest);

    ClosestPtPoint2(&passThroughPointToCubeSpace2, &obb, &closest2);
    ClosestPtPoint3(&passThroughPointToCubeSpace3, &obb, &closest3);
    // to multiply a vec with a matrix u must use vec4// the 1 lets u kepp the transformation in the local bx bject
    //let me block this line of code cus it could be changing the value gotten from nsd functn
    //closest = glm::vec3(mEntities[25]->getWorldMatrix() * glm::vec4(closest.x, closest.y, closest.z, 1.0f));
    //std::cout << "passThroughPointToCubeSpace outside of function is ("<< passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") " << std::endl;

    //std::cout << "           ___2.____ closest point on box just outside the ClosestPtPoint() fuction is: (" << closest.x << ", " << closest.y << ", " << closest.z << ") " << std::endl; //****************************************************
    /*
    float x;
    float y;
    float z;
    */
    //depends on if closest is in th box's model space & it is so good**************************************************************
    //access playerCube, order is important when multiplying matices
    //i need to say it's a vector explicitly so i usd vec4( , , ,0.0f), it's the only Way vector to multiply with a matrix
    //glm::vec4 conelocalHaxisToCubeSpace = mEntities[25]->getWorldMatrix() * (mEntities[24]->getWorldMatrix() * glm::vec4(0.0f, 1.0f, 0.0f,0.0f));
    //MIGHT NEED TO CHANGE THE LAST PARAMETER TO 1 SO THAT TRANSFORMATIONS AFFECT THE VECTOR---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //glm::vec3 conelocalHaxisToCubeSpace = glm::vec3(mEntities[25]->getWorldMatrix() * (mEntities[24]->getWorldMatrix() * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f)));
    //^                                   //might have to turn it into a vect3 befor i multiply with identity matrix if this doesnt work************************************************************************************************************

    //glm::vec3 conelocalHaxisToCubeSpace = glm::vec3(mEntities[25]->getWorldMatrix() * (glm::mat4(1.0f) * (mEntities[24]->getWorldMatrix() * glm::vec4(0.0f, 1.0f, 0.0f, 1.0f))));   //il need to change this 4 later//has to be 1 cus i use it for distance calculation
    //^^^^^^
    //use 1 for w initially then 0.0f for the other statements cus we dont want to change it afterwards. If u do it all in one line all the statement will function with the w being 1
    //glm::vec3 coneHLocalCurrently = glm::vec3(mEntities[24]->getOrientation() * glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
    //i need to chnage mEntities[25]->getWorldMatrix() to orientation **********************************************************************************************
    //glm::vec3 conelocalHaxisToCubeSpace = glm::vec3(mEntities[25]->getOrientation() * (glm::mat4(1.0f) * glm::vec4(coneHLocalCurrently.x, coneHLocalCurrently.y, coneHLocalCurrently.z, 0.0f)));
    
    //glm::vec3 conelocalHaxisToCubeSpace = glm::vec3(mEntities[24]->getOrientation() * glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));

    glm::vec3 conelocalHaxisToCubeSpace = (converterVector + glm::vec3(0.0f, 0.0f, 1.0f)) - converterVector;

    glm::vec3 conelocalHaxisToCubeSpace2 = (converterVector2 + glm::vec3(0.0f, 0.0f, 1.0f)) - converterVector2;
    glm::vec3 conelocalHaxisToCubeSpace3 = (converterVector3 + glm::vec3(0.0f, 0.0f, 1.0f)) - converterVector3;
    //glm::vec3 conelocalHaxisToCubeSpace = glm::vec3(glm::inverse(mEntities[24]->getWorldMatrix()) * (glm::vec4(0.0f, 0.0f, 1.0f, 1.0f)));//either 1.0f or 0.0f

    //MIGHT NEED TO CHANGE THE LAST PARAMETER TO 1 SO THAT TRANSFORMATIONS AFFECT THE VECTOR---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //glm::vec3 conelocalVaxisToCubeSpace = glm::vec3(mEntities[25]->getWorldMatrix() * (mEntities[24]->getWorldMatrix() * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f)));
    //^                                    //might have to turn it into a vect3 befor i multiply with identity matrix if this doesnt work********************************************************************************************************
    //glm::vec3 conelocalVaxisToCubeSpace = glm::vec3(mEntities[25]->getWorldMatrix() * (glm::mat4(1.0f) * (mEntities[24]->getWorldMatrix() * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f))));    //il need to change this 4 later//has to be 1 cus i use it for distance calculation
    //^^^^^^
    //use 1 for w initially then 0.0f for the other statements cus we dont want to change it afterwards. If u do it all in one line all the statement will function with the w being 1
    //glm::vec3 coneVLocalCurrently = glm::vec3(mEntities[24]->getOrientation() * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
    //i need to chnage mEntities[25]->getWorldMatrix() to orientation **********************************************************************************************
    //glm::vec3 conelocalVaxisToCubeSpace = glm::vec3(mEntities[25]->getOrientation() * (glm::mat4(1.0f) * glm::vec4(coneVLocalCurrently.x, coneVLocalCurrently.y, coneVLocalCurrently.z, 0.0f)));
    
    //glm::vec3 conelocalVaxisToCubeSpace = glm::vec3(mEntities[24]->getOrientation() * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));

    //glm::vec3 conelocalVaxisToCubeSpace = glm::vec3(glm::inverse(mEntities[24]->getWorldMatrix()) * (glm::vec4(1.0f, 0.0f, 0.0f, 0.0f)));//either 1.0f or 0.0f
    glm::vec3 conelocalVaxisToCubeSpace = (converterVector + glm::vec3(1.0f, 0.0f, 0.0f)) - converterVector;

    glm::vec3 conelocalVaxisToCubeSpace2 = (converterVector2 + glm::vec3(1.0f, 0.0f, 0.0f)) - converterVector2;
    glm::vec3 conelocalVaxisToCubeSpace3 = (converterVector3 + glm::vec3(1.0f, 0.0f, 0.0f)) - converterVector3;

    //std::cout << "           ___.____ conelocalHaxisToCubeSpace is: (" << conelocalHaxisToCubeSpace.x << ", " << conelocalHaxisToCubeSpace.y << ", " << conelocalHaxisToCubeSpace.z << ") " << std::endl;
    //std::cout << "           ___.____ conelocalVaxisToCubeSpace is: (" << conelocalVaxisToCubeSpace.x << ", " << conelocalVaxisToCubeSpace.y << ", " << conelocalVaxisToCubeSpace.z << ") " << std::endl;
    
    //perpendicular to vertical axes of cone
    //utilizing the equation of a plane formula
    /*
    float HplaneEquation = conelocalHaxisToCubeSpace.x * (x - closest.x) + conelocalHaxisToCubeSpace.y * (y - closest.y) + conelocalHaxisToCubeSpace.z * (z - closest.z);
    //perpendicular to horizontal axes of cone
    float VplaneEquation = conelocalVaxisToCubeSpace.x * (x - closest.x) + conelocalVaxisToCubeSpace.y * (y - closest.y) + conelocalVaxisToCubeSpace.z * (z - closest.z);
    */
    // Find the point on the plane that is closest to the cone position 
   // convert cone point to cube object space
    /*
    glm::vec3 positionOfCone = mEntities[10]->getPosition();
    glm::vec4 positionOfConeAsVectorExplicitly = glm::vec4(positionOfCone.x, positionOfCone.y, positionOfCone.z, 0);
    glm::vec4 passThroughPointToCubeSpace = mEntities[11]->getWorldMatrix() * positionOfConeAsVectorExplicitly;
    */
    // **t is already used else where as a static flaot variable so change it
    /*
    //soon to be deleted
    float tEquationVariable;
    float xAxisLineEquattion = passThroughPointToCubeSpace.x + (conelocalHaxisToCubeSpace.x * tEquationVariable);
    float yAxisLineEquattion = passThroughPointToCubeSpace.y + (conelocalHaxisToCubeSpace.y * tEquationVariable);
    float zAxisLineEquattion = passThroughPointToCubeSpace.z + (conelocalHaxisToCubeSpace.z * tEquationVariable);
    //i need to isolate for t on paper here actually then type it here for it t be computed
    float HplaneEquationTValue = conelocalHaxisToCubeSpace.x * (xAxisLineEquattion - closest.x) + conelocalHaxisToCubeSpace.y * (yAxisLineEquattion - closest.y) + conelocalHaxisToCubeSpace.z * (zAxisLineEquattion - closest.z);
    */
    //THS DOESNT TAKE THE ACTUALLY CLOSSEST POIN
    //float HplaneEquationTValue = ((conelocalHaxisToCubeSpace.x * passThroughPointToCubeSpace.x) - (conelocalHaxisToCubeSpace.x * closest.x) + (conelocalHaxisToCubeSpace.y * passThroughPointToCubeSpace.y) - (conelocalHaxisToCubeSpace.y * closest.y) + (conelocalHaxisToCubeSpace.z * passThroughPointToCubeSpace.z) - (conelocalHaxisToCubeSpace.z * closest.z)) / ((-1) * ((2 * conelocalHaxisToCubeSpace.x) + (2 * conelocalHaxisToCubeSpace.y) + (2 * conelocalHaxisToCubeSpace.z)));
    
    //std::cout << "           outside jj/passThroughPointToCubeSpace  befor being put in plane equation = (" << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") "<< std::endl;
    double HplaneEquationTValue = ((conelocalHaxisToCubeSpace.x * closest.x) + (conelocalHaxisToCubeSpace.y * closest.y) + (conelocalHaxisToCubeSpace.z * closest.z) - (conelocalHaxisToCubeSpace.x * passThroughPointToCubeSpace.x) - (conelocalHaxisToCubeSpace.y * passThroughPointToCubeSpace.y) - (conelocalHaxisToCubeSpace.z * passThroughPointToCubeSpace.z)) / ((conelocalHaxisToCubeSpace.x * conelocalHaxisToCubeSpace.x) + (conelocalHaxisToCubeSpace.y * conelocalHaxisToCubeSpace.y) + (conelocalHaxisToCubeSpace.z * conelocalHaxisToCubeSpace.z));
    double VplaneEquationTValue = ((conelocalVaxisToCubeSpace.x * closest.x) + (conelocalVaxisToCubeSpace.y * closest.y) + (conelocalVaxisToCubeSpace.z * closest.z) - (conelocalVaxisToCubeSpace.x * passThroughPointToCubeSpace.x) - (conelocalVaxisToCubeSpace.y * passThroughPointToCubeSpace.y) - (conelocalVaxisToCubeSpace.z * passThroughPointToCubeSpace.z)) / ((conelocalVaxisToCubeSpace.x * conelocalVaxisToCubeSpace.x) + (conelocalVaxisToCubeSpace.y * conelocalVaxisToCubeSpace.y) + (conelocalVaxisToCubeSpace.z * conelocalVaxisToCubeSpace.z));


    double HplaneEquationTValue2 = ((conelocalHaxisToCubeSpace2.x * closest2.x) + (conelocalHaxisToCubeSpace2.y * closest2.y) + (conelocalHaxisToCubeSpace2.z * closest2.z) - (conelocalHaxisToCubeSpace2.x * passThroughPointToCubeSpace2.x) - (conelocalHaxisToCubeSpace2.y * passThroughPointToCubeSpace2.y) - (conelocalHaxisToCubeSpace2.z * passThroughPointToCubeSpace2.z)) / ((conelocalHaxisToCubeSpace2.x * conelocalHaxisToCubeSpace2.x) + (conelocalHaxisToCubeSpace2.y * conelocalHaxisToCubeSpace2.y) + (conelocalHaxisToCubeSpace2.z * conelocalHaxisToCubeSpace2.z));
    double VplaneEquationTValue2 = ((conelocalVaxisToCubeSpace2.x * closest2.x) + (conelocalVaxisToCubeSpace2.y * closest2.y) + (conelocalVaxisToCubeSpace2.z * closest2.z) - (conelocalVaxisToCubeSpace2.x * passThroughPointToCubeSpace2.x) - (conelocalVaxisToCubeSpace2.y * passThroughPointToCubeSpace2.y) - (conelocalVaxisToCubeSpace2.z * passThroughPointToCubeSpace2.z)) / ((conelocalVaxisToCubeSpace2.x * conelocalVaxisToCubeSpace2.x) + (conelocalVaxisToCubeSpace2.y * conelocalVaxisToCubeSpace2.y) + (conelocalVaxisToCubeSpace2.z * conelocalVaxisToCubeSpace2.z));

    double HplaneEquationTValue3 = ((conelocalHaxisToCubeSpace3.x * closest3.x) + (conelocalHaxisToCubeSpace3.y * closest3.y) + (conelocalHaxisToCubeSpace3.z * closest3.z) - (conelocalHaxisToCubeSpace3.x * passThroughPointToCubeSpace3.x) - (conelocalHaxisToCubeSpace3.y * passThroughPointToCubeSpace3.y) - (conelocalHaxisToCubeSpace3.z * passThroughPointToCubeSpace3.z)) / ((conelocalHaxisToCubeSpace3.x * conelocalHaxisToCubeSpace3.x) + (conelocalHaxisToCubeSpace3.y * conelocalHaxisToCubeSpace3.y) + (conelocalHaxisToCubeSpace3.z * conelocalHaxisToCubeSpace3.z));
    double VplaneEquationTValue3 = ((conelocalVaxisToCubeSpace3.x * closest3.x) + (conelocalVaxisToCubeSpace3.y * closest3.y) + (conelocalVaxisToCubeSpace3.z * closest3.z) - (conelocalVaxisToCubeSpace3.x * passThroughPointToCubeSpace3.x) - (conelocalVaxisToCubeSpace3.y * passThroughPointToCubeSpace3.y) - (conelocalVaxisToCubeSpace3.z * passThroughPointToCubeSpace3.z)) / ((conelocalVaxisToCubeSpace3.x * conelocalVaxisToCubeSpace3.x) + (conelocalVaxisToCubeSpace3.y * conelocalVaxisToCubeSpace3.y) + (conelocalVaxisToCubeSpace3.z * conelocalVaxisToCubeSpace3.z));






    // THE BEST WAY TO HANDLE A LOGIC ERROR IS TO PRINT STATEMENTS ON EVERYSTEP OF THE WAY TO SEE WHERE THE WRONGE RESULT STEMS FROM*******************************************
    
    
    //std::cout << "           top part of division is " << ((conelocalHaxisToCubeSpace.x * closest.x) + (conelocalHaxisToCubeSpace.y * closest.y) + (conelocalHaxisToCubeSpace.z * closest.z) - (conelocalHaxisToCubeSpace.x * passThroughPointToCubeSpace.x) - (conelocalHaxisToCubeSpace.y * passThroughPointToCubeSpace.y) - (conelocalHaxisToCubeSpace.z * passThroughPointToCubeSpace.z)) << std::endl;
    //std::cout << "           bottom part of division is " << ((conelocalHaxisToCubeSpace.x * conelocalHaxisToCubeSpace.x) + (conelocalHaxisToCubeSpace.y * conelocalHaxisToCubeSpace.y) + (conelocalHaxisToCubeSpace.z * conelocalHaxisToCubeSpace.z)) << std::endl;
    //std::cout << "           conelocalHaxisToCubeSpace.x * conelocalHaxisToCubeSpace.x(" << conelocalHaxisToCubeSpace.x * conelocalHaxisToCubeSpace.x << ") + conelocalHaxisToCubeSpace.y * conelocalHaxisToCubeSpace.y(" << conelocalHaxisToCubeSpace.y * conelocalHaxisToCubeSpace.y << ") + conelocalHaxisToCubeSpace.z * conelocalHaxisToCubeSpace.z(" << conelocalHaxisToCubeSpace.z * conelocalHaxisToCubeSpace.z <<")==" << HplaneEquationTValue << std::endl;
    //std::cout << "           t/HplaneEquationTValue = " << HplaneEquationTValue << std::endl;
    
    //std::cout << "               " << std::endl;
    
    double xAxisLineEquattionFinalValue = passThroughPointToCubeSpace.x + (conelocalHaxisToCubeSpace.x * HplaneEquationTValue);
    double yAxisLineEquattionFinalValue = passThroughPointToCubeSpace.y + (conelocalHaxisToCubeSpace.y * HplaneEquationTValue);
    double zAxisLineEquattionFinalValue = passThroughPointToCubeSpace.z + (conelocalHaxisToCubeSpace.z * HplaneEquationTValue);

    double xAxisLineEquattionVFinalValue = passThroughPointToCubeSpace.x + (conelocalVaxisToCubeSpace.x * VplaneEquationTValue);
    double yAxisLineEquattionVFinalValue = passThroughPointToCubeSpace.y + (conelocalVaxisToCubeSpace.y * VplaneEquationTValue);
    double zAxisLineEquattionVFinalValue = passThroughPointToCubeSpace.z + (conelocalVaxisToCubeSpace.z * VplaneEquationTValue);



    double xAxisLineEquattionFinalValue2 = passThroughPointToCubeSpace2.x + (conelocalHaxisToCubeSpace2.x * HplaneEquationTValue2);
    double yAxisLineEquattionFinalValue2 = passThroughPointToCubeSpace2.y + (conelocalHaxisToCubeSpace2.y * HplaneEquationTValue2);
    double zAxisLineEquattionFinalValue2 = passThroughPointToCubeSpace2.z + (conelocalHaxisToCubeSpace2.z * HplaneEquationTValue2);

    double xAxisLineEquattionVFinalValue2 = passThroughPointToCubeSpace2.x + (conelocalVaxisToCubeSpace2.x * VplaneEquationTValue2);
    double yAxisLineEquattionVFinalValue2 = passThroughPointToCubeSpace2.y + (conelocalVaxisToCubeSpace2.y * VplaneEquationTValue2);
    double zAxisLineEquattionVFinalValue2 = passThroughPointToCubeSpace2.z + (conelocalVaxisToCubeSpace2.z * VplaneEquationTValue2);



    double xAxisLineEquattionFinalValue3 = passThroughPointToCubeSpace3.x + (conelocalHaxisToCubeSpace3.x * HplaneEquationTValue3);
    double yAxisLineEquattionFinalValue3 = passThroughPointToCubeSpace3.y + (conelocalHaxisToCubeSpace3.y * HplaneEquationTValue3);
    double zAxisLineEquattionFinalValue3 = passThroughPointToCubeSpace3.z + (conelocalHaxisToCubeSpace3.z * HplaneEquationTValue3);

    double xAxisLineEquattionVFinalValue3 = passThroughPointToCubeSpace3.x + (conelocalVaxisToCubeSpace3.x * VplaneEquationTValue3);
    double yAxisLineEquattionVFinalValue3 = passThroughPointToCubeSpace3.y + (conelocalVaxisToCubeSpace3.y * VplaneEquationTValue3);
    double zAxisLineEquattionVFinalValue3 = passThroughPointToCubeSpace3.z + (conelocalVaxisToCubeSpace3.z * VplaneEquationTValue3);

    //std::cout << "      y equation that t goes into: yAxisLineEquattionFinalValue = " << passThroughPointToCubeSpace.y << " + (" << "passThroughPointToCubeSpace.y + " << HplaneEquationTValue << "). Answer is " << xAxisLineEquattionFinalValue << std::endl;
    //^
    //std::cout << "[box position global object space?: (" << mEntities[25]->getPosition().x << ", " << mEntities[25]->getPosition().y << ", " << mEntities[25]->getPosition().z << ")" << std::endl;
    

    //std::cout << "cone position in box object space: (" << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ")" << std::endl;

    
    //std::cout << "orgin of cone cone local H axis in box Space is: (" << conelocalHaxisToCubeSpace.x << ", " << conelocalHaxisToCubeSpace.y << ", " << conelocalHaxisToCubeSpace.z << ")" << std::endl;
    glm::vec3 CubeSpaceHPlaneClosestPoint = glm::vec3(xAxisLineEquattionFinalValue, yAxisLineEquattionFinalValue, zAxisLineEquattionFinalValue);
    glm::vec3 CubeSpaceVPlaneClosestPoint = glm::vec3(xAxisLineEquattionVFinalValue, yAxisLineEquattionVFinalValue, zAxisLineEquattionVFinalValue);



    glm::vec3 CubeSpaceHPlaneClosestPoint2 = glm::vec3(xAxisLineEquattionFinalValue2, yAxisLineEquattionFinalValue2, zAxisLineEquattionFinalValue2);
    glm::vec3 CubeSpaceVPlaneClosestPoint2 = glm::vec3(xAxisLineEquattionVFinalValue2, yAxisLineEquattionVFinalValue2, zAxisLineEquattionVFinalValue2);


    glm::vec3 CubeSpaceHPlaneClosestPoint3 = glm::vec3(xAxisLineEquattionFinalValue3, yAxisLineEquattionFinalValue3, zAxisLineEquattionFinalValue3);
    glm::vec3 CubeSpaceVPlaneClosestPoint3 = glm::vec3(xAxisLineEquattionVFinalValue3, yAxisLineEquattionVFinalValue3, zAxisLineEquattionVFinalValue3);



    //std::cout << "                      [] CubeSpaceHPlaneClosestPoint is : (" << CubeSpaceHPlaneClosestPoint.x << ", " << CubeSpaceHPlaneClosestPoint.y << ", " << CubeSpaceHPlaneClosestPoint.z << ") " << std::endl;
    //std::cout << "                      [] CubeSpaceVPlaneClosestPoint is : (" << CubeSpaceVPlaneClosestPoint.x << ", " << CubeSpaceVPlaneClosestPoint.y << ", " << CubeSpaceVPlaneClosestPoint.z << ") " << std::endl;
    //^


    double distToPlaneClosestPoint = sqrt(pow(CubeSpaceHPlaneClosestPoint.x - passThroughPointToCubeSpace.x, 2) + pow(CubeSpaceHPlaneClosestPoint.y - passThroughPointToCubeSpace.y, 2) + pow(CubeSpaceHPlaneClosestPoint.z - passThroughPointToCubeSpace.z, 2));
    double vDistToPlaneClosestPoint = sqrt(pow(CubeSpaceVPlaneClosestPoint.x - passThroughPointToCubeSpace.x, 2) + pow(CubeSpaceVPlaneClosestPoint.y - passThroughPointToCubeSpace.y, 2) + pow(CubeSpaceVPlaneClosestPoint.z - passThroughPointToCubeSpace.z, 2));
    
    //check if distToPlaneClosestPoint is less than height of cone

    double distToPlaneClosestPoint2 = sqrt(pow(CubeSpaceHPlaneClosestPoint2.x - passThroughPointToCubeSpace2.x, 2) + pow(CubeSpaceHPlaneClosestPoint2.y - passThroughPointToCubeSpace2.y, 2) + pow(CubeSpaceHPlaneClosestPoint2.z - passThroughPointToCubeSpace2.z, 2));
    double vDistToPlaneClosestPoint2 = sqrt(pow(CubeSpaceVPlaneClosestPoint2.x - passThroughPointToCubeSpace2.x, 2) + pow(CubeSpaceVPlaneClosestPoint2.y - passThroughPointToCubeSpace2.y, 2) + pow(CubeSpaceVPlaneClosestPoint2.z - passThroughPointToCubeSpace2.z, 2));

    double distToPlaneClosestPoint3 = sqrt(pow(CubeSpaceHPlaneClosestPoint3.x - passThroughPointToCubeSpace3.x, 2) + pow(CubeSpaceHPlaneClosestPoint3.y - passThroughPointToCubeSpace3.y, 2) + pow(CubeSpaceHPlaneClosestPoint3.z - passThroughPointToCubeSpace3.z, 2));
    double vDistToPlaneClosestPoint3 = sqrt(pow(CubeSpaceVPlaneClosestPoint3.x - passThroughPointToCubeSpace3.x, 2) + pow(CubeSpaceVPlaneClosestPoint3.y - passThroughPointToCubeSpace3.y, 2) + pow(CubeSpaceVPlaneClosestPoint3.z - passThroughPointToCubeSpace3.z, 2));




    //std::cout << "                                          [cone position global space: (" << mEntities[24]->getPosition().x << ", " << mEntities[24]->getPosition().y << ", " << mEntities[24]->getPosition().z << ") but distance to plane's closest point is: (" << distToPlaneClosestPoint << ")" << std::endl;
    //std::cout << "               " << std::endl;
    //std::cout << "      CubeSpaceHPlaneClosestPoint.y(" << CubeSpaceHPlaneClosestPoint.y << ") >=  passThroughPointToCubeSpace.y(" << passThroughPointToCubeSpace.y << ") " << std::endl;
    //^
    int inRange = 0;
    int posibleCollision;

    int inRange2 = 0;
    int posibleCollision2;

    int inRange3 = 0;
    int posibleCollision3;


    if ((distToPlaneClosestPoint <= coneMeshHeight) && (CubeSpaceHPlaneClosestPoint.z <= passThroughPointToCubeSpace.z)) {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl; 
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    
        inRange++;
    }
    else {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }
    if (vDistToPlaneClosestPoint <= coneMeshRadius) {
        //std::cout << "********************************************************************************************************" << std::endl;
        //std::cout << "********************************************************************************************************" << std::endl;
        //std::cout << "[*******************************]distToPlaneClosestPoint is less than radus of cone & in the range of cone!!,distToPlaneClosestPoint is: " << vDistToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshRadius << "********" << std::endl;
        //^
        inRange++;
    }
    else {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }
    if (inRange == 2) {

        //closest - passThroughPointToCubeSpace 
        /*
        std::cout << "===============================================================================================================================================================" << std::endl;
        std::cout << "===============================================================================================================================================================" << std::endl;
        std::cout << "===============================================================================================================================================================" << std::endl;
        */
        //find out how close the first point is to the orgin
        glm::vec3 bPTP = CubeSpaceVPlaneClosestPoint - passThroughPointToCubeSpace;
        //std::cout << "CubeSpaceHPlaneClosestPoin - passThroughPointToCubeSpace  is (" << CubeSpaceVPlaneClosestPoint.x << ", " << CubeSpaceVPlaneClosestPoint.y << ", " << CubeSpaceVPlaneClosestPoint.z << ") - " << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") wwhch is bPTP "<< std::endl;
        //std::cout << "bPTP is (" << bPTP.x << ", " << bPTP.y << ", " << bPTP.z << ") " << std::endl;
        glm::vec3 unitOfbPTP = glm::vec3(bPTP.x/ vDistToPlaneClosestPoint, bPTP.y / vDistToPlaneClosestPoint, bPTP.z / vDistToPlaneClosestPoint);
        //std::cout << "unitOfbPTP is (" << unitOfbPTP.x << ", " << unitOfbPTP.y << ", " << unitOfbPTP.z << ") "<< std::endl;

        glm::vec3 fullVectorFromUnitOfbPTP = (unitOfbPTP * coneMeshRadius) + passThroughPointToCubeSpace;
        //std::cout << "fullVectorFromUnitOfbPTP is (" << fullVectorFromUnitOfbPTP.x << ", " << fullVectorFromUnitOfbPTP.y << ", " << fullVectorFromUnitOfbPTP.z << ") " << std::endl;


        //(mEntities[24]->getPosition() + glm::vec3(0.0f, 0.0f, coneMeshHeight));
        // the derection doesn't matter for this rn, if i find out it does latte on, come back to here **********************
        glm::vec3 topOfCone = passThroughPointToCubeSpace - glm::vec3(0.0f, 0.0f, coneMeshHeight) ;
        //std::cout << "topOfCone is (" << topOfCone.x << ", " << topOfCone.y << ", " << topOfCone.z << ") " << std::endl;

        glm::vec3 segmentOfConeP = fullVectorFromUnitOfbPTP - topOfCone;
        //std::cout << "segmentOfConeP is (" << segmentOfConeP.x << ", " << segmentOfConeP.y << ", " << segmentOfConeP.z << ") " << std::endl;
        //find the perpendicular vector
        double x = 3.0;
        double y = 4.0;
        //double PQ = segmentOfConeP.x * x + segmentOfConeP.y * y + segmentOfConeP.z * z;

        double z = (segmentOfConeP.x * x + segmentOfConeP.y * y) / (-segmentOfConeP.z);
        //std::cout << "z is (" << z << ") " << std::endl;
        glm::vec3 newPerpendicularVector = glm::vec3(x, y, z);
        //std::cout << "newPerpendicularVector is (" << newPerpendicularVector.x << ", " << newPerpendicularVector.y << ", " << newPerpendicularVector.z << ") " << std::endl;
        double newplaneEquationTValue = ((newPerpendicularVector.x * fullVectorFromUnitOfbPTP.x) + (newPerpendicularVector.y * fullVectorFromUnitOfbPTP.y) + (newPerpendicularVector.z * fullVectorFromUnitOfbPTP.z) - (newPerpendicularVector.x * closest.x) - (newPerpendicularVector.y * closest.y) - (newPerpendicularVector.z * closest.z)) / ((newPerpendicularVector.x * newPerpendicularVector.x) + (newPerpendicularVector.y * newPerpendicularVector.y) + (newPerpendicularVector.z * newPerpendicularVector.z));
        //std::cout << "newplaneEquationTValue is (" << newplaneEquationTValue << ") " << std::endl;

        double xAxisNewLineEquattionFinalValue = closest.x + (newPerpendicularVector.x * newplaneEquationTValue);
        double yAxisNewLineEquattionFinalValue = closest.y + (newPerpendicularVector.y * newplaneEquationTValue);
        double zAxisNewLineEquattionFinalValue = closest.z + (newPerpendicularVector.z * newplaneEquationTValue);

        glm::vec3 HSegmentClosestPoint = glm::vec3(xAxisNewLineEquattionFinalValue, yAxisNewLineEquattionFinalValue, zAxisNewLineEquattionFinalValue);
        //std::cout << "HSegmentClosestPoint is (" << HSegmentClosestPoint.x << ", " << HSegmentClosestPoint.y << ", " << HSegmentClosestPoint.z << ") " << std::endl;
        
        double middleDistanceToOrgin = sqrt(pow(closest.x - passThroughPointToCubeSpace.x, 2) + pow(closest.y - passThroughPointToCubeSpace.y, 2) + pow(closest.z - passThroughPointToCubeSpace.z, 2));
        //std::cout << "middleDistanceToOrgin is (" << middleDistanceToOrgin << ") " << std::endl;
        double middleSegmentDistanceToOrgin = sqrt(pow(HSegmentClosestPoint.x - passThroughPointToCubeSpace.x, 2) + pow(HSegmentClosestPoint.y - passThroughPointToCubeSpace.y, 2) + pow(HSegmentClosestPoint.z - passThroughPointToCubeSpace.z, 2));
        //std::cout << "middleSegmentDistanceToOrgin is (" << middleSegmentDistanceToOrgin << ") " << std::endl;

        if (middleDistanceToOrgin <= middleSegmentDistanceToOrgin) {
            std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@There is actually a collision th enemy vision #1 so you lose" << std::endl;
        }

        //find the closest point to the segment line of the cone
    }


    //22222222222222222222222222222222222222222222222222222222
    if ((distToPlaneClosestPoint2 <= coneMeshHeight) && (CubeSpaceHPlaneClosestPoint2.z <= passThroughPointToCubeSpace2.z)) {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl; 
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;

        inRange2++;
    }
    else {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }
    if (vDistToPlaneClosestPoint2 <= coneMeshRadius) {
        //std::cout << "********************************************************************************************************" << std::endl;
        //std::cout << "********************************************************************************************************" << std::endl;
        //std::cout << "[*******************************]distToPlaneClosestPoint is less than radus of cone & in the range of cone!!,distToPlaneClosestPoint is: " << vDistToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshRadius << "********" << std::endl;
        //^
        inRange2++;
    }
    else {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }
    if (inRange2 == 2) {

        //closest - passThroughPointToCubeSpace 
        /*
        std::cout << "===============================================================================================================================================================" << std::endl;
        std::cout << "===============================================================================================================================================================" << std::endl;
        std::cout << "===============================================================================================================================================================" << std::endl;
        */
        //find out how close the first point is to the orgin
        glm::vec3 bPTP = CubeSpaceVPlaneClosestPoint2 - passThroughPointToCubeSpace2;
        //std::cout << "CubeSpaceHPlaneClosestPoin - passThroughPointToCubeSpace  is (" << CubeSpaceVPlaneClosestPoint.x << ", " << CubeSpaceVPlaneClosestPoint.y << ", " << CubeSpaceVPlaneClosestPoint.z << ") - " << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") wwhch is bPTP "<< std::endl;
        //std::cout << "bPTP is (" << bPTP.x << ", " << bPTP.y << ", " << bPTP.z << ") " << std::endl;
        glm::vec3 unitOfbPTP = glm::vec3(bPTP.x / vDistToPlaneClosestPoint2, bPTP.y / vDistToPlaneClosestPoint2, bPTP.z / vDistToPlaneClosestPoint2);
        //std::cout << "unitOfbPTP is (" << unitOfbPTP.x << ", " << unitOfbPTP.y << ", " << unitOfbPTP.z << ") "<< std::endl;

        glm::vec3 fullVectorFromUnitOfbPTP = (unitOfbPTP * coneMeshRadius) + passThroughPointToCubeSpace2;
        //std::cout << "fullVectorFromUnitOfbPTP is (" << fullVectorFromUnitOfbPTP.x << ", " << fullVectorFromUnitOfbPTP.y << ", " << fullVectorFromUnitOfbPTP.z << ") " << std::endl;


        //(mEntities[24]->getPosition() + glm::vec3(0.0f, 0.0f, coneMeshHeight));
        // the derection doesn't matter for this rn, if i find out it does latte on, come back to here **********************
        //THIS WILL CAUSE ME TROUBLE LATTER
        glm::vec3 topOfCone = passThroughPointToCubeSpace2 - glm::vec3(0.0f, 0.0f, coneMeshHeight);
        //std::cout << "topOfCone is (" << topOfCone.x << ", " << topOfCone.y << ", " << topOfCone.z << ") " << std::endl;

        glm::vec3 segmentOfConeP = fullVectorFromUnitOfbPTP - topOfCone;
        //std::cout << "segmentOfConeP is (" << segmentOfConeP.x << ", " << segmentOfConeP.y << ", " << segmentOfConeP.z << ") " << std::endl;
        //find the perpendicular vector
        double x = 3.0;
        double y = 4.0;
        //double PQ = segmentOfConeP.x * x + segmentOfConeP.y * y + segmentOfConeP.z * z;

        double z = (segmentOfConeP.x * x + segmentOfConeP.y * y) / (-segmentOfConeP.z);
        //std::cout << "z is (" << z << ") " << std::endl;
        glm::vec3 newPerpendicularVector = glm::vec3(x, y, z);
        //std::cout << "newPerpendicularVector is (" << newPerpendicularVector.x << ", " << newPerpendicularVector.y << ", " << newPerpendicularVector.z << ") " << std::endl;
        double newplaneEquationTValue = ((newPerpendicularVector.x * fullVectorFromUnitOfbPTP.x) + (newPerpendicularVector.y * fullVectorFromUnitOfbPTP.y) + (newPerpendicularVector.z * fullVectorFromUnitOfbPTP.z) - (newPerpendicularVector.x * closest2.x) - (newPerpendicularVector.y * closest2.y) - (newPerpendicularVector.z * closest2.z)) / ((newPerpendicularVector.x * newPerpendicularVector.x) + (newPerpendicularVector.y * newPerpendicularVector.y) + (newPerpendicularVector.z * newPerpendicularVector.z));
        //std::cout << "newplaneEquationTValue is (" << newplaneEquationTValue << ") " << std::endl;

        double xAxisNewLineEquattionFinalValue = closest2.x + (newPerpendicularVector.x * newplaneEquationTValue);
        double yAxisNewLineEquattionFinalValue = closest2.y + (newPerpendicularVector.y * newplaneEquationTValue);
        double zAxisNewLineEquattionFinalValue = closest2.z + (newPerpendicularVector.z * newplaneEquationTValue);

        glm::vec3 HSegmentClosestPoint = glm::vec3(xAxisNewLineEquattionFinalValue, yAxisNewLineEquattionFinalValue, zAxisNewLineEquattionFinalValue);
        //std::cout << "HSegmentClosestPoint is (" << HSegmentClosestPoint.x << ", " << HSegmentClosestPoint.y << ", " << HSegmentClosestPoint.z << ") " << std::endl;

        double middleDistanceToOrgin = sqrt(pow(closest.x - passThroughPointToCubeSpace2.x, 2) + pow(closest2.y - passThroughPointToCubeSpace2.y, 2) + pow(closest2.z - passThroughPointToCubeSpace2.z, 2));
        //std::cout << "middleDistanceToOrgin is (" << middleDistanceToOrgin << ") " << std::endl;
        double middleSegmentDistanceToOrgin = sqrt(pow(HSegmentClosestPoint.x - passThroughPointToCubeSpace2.x, 2) + pow(HSegmentClosestPoint.y - passThroughPointToCubeSpace2.y, 2) + pow(HSegmentClosestPoint.z - passThroughPointToCubeSpace2.z, 2));
        //std::cout << "middleSegmentDistanceToOrgin is (" << middleSegmentDistanceToOrgin << ") " << std::endl;

        if (middleDistanceToOrgin <= middleSegmentDistanceToOrgin) {
            std::cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$@There is actually a collision with enemy vision #2 so you lose" << std::endl;
        }

        //find the closest point to the segment line of the cone
    }














    //33333333333333333333333333333333333333333333333333333333333333333333333
    if ((distToPlaneClosestPoint3 <= coneMeshHeight) && (CubeSpaceHPlaneClosestPoint3.z <= passThroughPointToCubeSpace3.z)) {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl; 
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;

        inRange3++;
    }
    else {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }
    if (vDistToPlaneClosestPoint3 <= coneMeshRadius) {
        //std::cout << "********************************************************************************************************" << std::endl;
        //std::cout << "********************************************************************************************************" << std::endl;
        //std::cout << "[*******************************]distToPlaneClosestPoint is less than radus of cone & in the range of cone!!,distToPlaneClosestPoint is: " << vDistToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshRadius << "********" << std::endl;
        //^
        inRange3++;
    }
    else {
        //std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!distToPlaneClosestPoint is more than height of cone!!,distToPlaneClosestPoint is: " << distToPlaneClosestPoint << ", coneMeshHeight: " << coneMeshHeight << "!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
    }
    if (inRange3 == 2) {

        //closest - passThroughPointToCubeSpace 
        /*
        std::cout << "===============================================================================================================================================================" << std::endl;
        std::cout << "===============================================================================================================================================================" << std::endl;
        std::cout << "===============================================================================================================================================================" << std::endl;
        */
        //find out how close the first point is to the orgin
        glm::vec3 bPTP = CubeSpaceVPlaneClosestPoint3 - passThroughPointToCubeSpace3;
        //std::cout << "CubeSpaceHPlaneClosestPoin - passThroughPointToCubeSpace  is (" << CubeSpaceVPlaneClosestPoint.x << ", " << CubeSpaceVPlaneClosestPoint.y << ", " << CubeSpaceVPlaneClosestPoint.z << ") - " << passThroughPointToCubeSpace.x << ", " << passThroughPointToCubeSpace.y << ", " << passThroughPointToCubeSpace.z << ") wwhch is bPTP "<< std::endl;
        //std::cout << "bPTP is (" << bPTP.x << ", " << bPTP.y << ", " << bPTP.z << ") " << std::endl;
        glm::vec3 unitOfbPTP = glm::vec3(bPTP.x / vDistToPlaneClosestPoint3, bPTP.y / vDistToPlaneClosestPoint3, bPTP.z / vDistToPlaneClosestPoint3);
        //std::cout << "unitOfbPTP is (" << unitOfbPTP.x << ", " << unitOfbPTP.y << ", " << unitOfbPTP.z << ") "<< std::endl;

        glm::vec3 fullVectorFromUnitOfbPTP = (unitOfbPTP * coneMeshRadius) + passThroughPointToCubeSpace3;
        //std::cout << "fullVectorFromUnitOfbPTP is (" << fullVectorFromUnitOfbPTP.x << ", " << fullVectorFromUnitOfbPTP.y << ", " << fullVectorFromUnitOfbPTP.z << ") " << std::endl;


        //(mEntities[24]->getPosition() + glm::vec3(0.0f, 0.0f, coneMeshHeight));
        // the derection doesn't matter for this rn, if i find out it does latte on, come back to here **********************
        //THIS WILL CAUSE ME TROUBLE LATTER
        glm::vec3 topOfCone = passThroughPointToCubeSpace3 - glm::vec3(0.0f, 0.0f, coneMeshHeight);
        //std::cout << "topOfCone is (" << topOfCone.x << ", " << topOfCone.y << ", " << topOfCone.z << ") " << std::endl;

        glm::vec3 segmentOfConeP = fullVectorFromUnitOfbPTP - topOfCone;
        //std::cout << "segmentOfConeP is (" << segmentOfConeP.x << ", " << segmentOfConeP.y << ", " << segmentOfConeP.z << ") " << std::endl;
        //find the perpendicular vector
        double x = 3.0;
        double y = 4.0;
        //double PQ = segmentOfConeP.x * x + segmentOfConeP.y * y + segmentOfConeP.z * z;

        double z = (segmentOfConeP.x * x + segmentOfConeP.y * y) / (-segmentOfConeP.z);
        //std::cout << "z is (" << z << ") " << std::endl;
        glm::vec3 newPerpendicularVector = glm::vec3(x, y, z);
        //std::cout << "newPerpendicularVector is (" << newPerpendicularVector.x << ", " << newPerpendicularVector.y << ", " << newPerpendicularVector.z << ") " << std::endl;
        double newplaneEquationTValue = ((newPerpendicularVector.x * fullVectorFromUnitOfbPTP.x) + (newPerpendicularVector.y * fullVectorFromUnitOfbPTP.y) + (newPerpendicularVector.z * fullVectorFromUnitOfbPTP.z) - (newPerpendicularVector.x * closest3.x) - (newPerpendicularVector.y * closest3.y) - (newPerpendicularVector.z * closest3.z)) / ((newPerpendicularVector.x * newPerpendicularVector.x) + (newPerpendicularVector.y * newPerpendicularVector.y) + (newPerpendicularVector.z * newPerpendicularVector.z));
        //std::cout << "newplaneEquationTValue is (" << newplaneEquationTValue << ") " << std::endl;

        double xAxisNewLineEquattionFinalValue = closest3.x + (newPerpendicularVector.x * newplaneEquationTValue);
        double yAxisNewLineEquattionFinalValue = closest3.y + (newPerpendicularVector.y * newplaneEquationTValue);
        double zAxisNewLineEquattionFinalValue = closest3.z + (newPerpendicularVector.z * newplaneEquationTValue);

        glm::vec3 HSegmentClosestPoint = glm::vec3(xAxisNewLineEquattionFinalValue, yAxisNewLineEquattionFinalValue, zAxisNewLineEquattionFinalValue);
        //std::cout << "HSegmentClosestPoint is (" << HSegmentClosestPoint.x << ", " << HSegmentClosestPoint.y << ", " << HSegmentClosestPoint.z << ") " << std::endl;

        double middleDistanceToOrgin = sqrt(pow(closest3.x - passThroughPointToCubeSpace3.x, 2) + pow(closest3.y - passThroughPointToCubeSpace3.y, 2) + pow(closest3.z - passThroughPointToCubeSpace3.z, 2));
        //std::cout << "middleDistanceToOrgin is (" << middleDistanceToOrgin << ") " << std::endl;
        double middleSegmentDistanceToOrgin = sqrt(pow(HSegmentClosestPoint.x - passThroughPointToCubeSpace3.x, 2) + pow(HSegmentClosestPoint.y - passThroughPointToCubeSpace3.y, 2) + pow(HSegmentClosestPoint.z - passThroughPointToCubeSpace3.z, 2));
        //std::cout << "middleSegmentDistanceToOrgin is (" << middleSegmentDistanceToOrgin << ") " << std::endl;

        if (middleDistanceToOrgin <= middleSegmentDistanceToOrgin) {
            std::cout << "###################################################################################################################There is actually a collision with nmy vision #3 so you lose" << std::endl;
        }

        //find the closest point to the segment line of the cone
    }


    //check if closest is ...

    //float dist = Dot();

    //just this here for now
    //handleCollision(glm::vec3(-7, 5, -12));
    //make it true for now
    //return true;
}

//This should be in another class but il do that latter
void BasicSceneRenderer::handleCollision(glm::vec3 pointOfCollision) {

}

